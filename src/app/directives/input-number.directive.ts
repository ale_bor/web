import {Directive, ElementRef} from "@angular/core";

@Directive({
  selector: '[appInputNumber]'
})
export class InputNumberDirective {

  constructor(el: ElementRef) {
    el.nativeElement.addEventListener('blur', (el) => {
      let regExp = new RegExp(/-?\d*\.?\d*/);
      let target = el.target;
      let matches = target.value.match(regExp);
      target.value = matches && matches[0] ? matches[0] : '';
    });
  }

}
