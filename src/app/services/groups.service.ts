import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {Group} from "../models/group";
import {BaseHttpHelper} from "../helpers/base-http.helper";
import {Router} from "@angular/router";
import {handleData, process, LoadingListener} from "../helpers/response.helper";

@Injectable()
export class GroupsService {

  constructor(private http: Http, private router: Router) {
  }

  groups(listener: LoadingListener): Observable<Group[]> {
    return process(new BaseHttpHelper<Group[]>(this.http, this.router, '/groups'), listener)
      .map(handleData);
  }

  group(listener: LoadingListener, id: number): Observable<Group> {
    return process(new BaseHttpHelper<Group>(this.http, this.router, `/groups/${id}`), listener)
      .map(handleData);
  }

  update(listener: LoadingListener, group: Group): Observable<Group> {
    return process(new BaseHttpHelper<Group>(this.http, this.router, `/groups/${group.id}`, group).put(), listener)
      .map(handleData);
  }

  create(listener: LoadingListener, group: Group): Observable<Group> {
    return process(new BaseHttpHelper<Group>(this.http, this.router, `/groups`, group).put(), listener)
      .map(handleData);
  }
}
