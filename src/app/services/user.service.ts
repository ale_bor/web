import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {UserDataModel} from "../models/user";
import {DefaultResult} from "../models/DefaultResult";
import {BaseHttpHelper} from "../helpers/base-http.helper";
import {process, LoadingListener} from "../helpers/response.helper";

export interface LoginFormModel {
  email: string;
  password: string;
}

@Injectable()
export class UserService {

  constructor(private http: Http,
              private router: Router) {
  }

  auth(model: LoginFormModel, listener: LoadingListener): Observable<DefaultResult<UserDataModel>> {
    let f = (res: DefaultResult<UserDataModel>) => {
      let authToken = res.meta.tokenString;
      localStorage.setItem('token', authToken);
      BaseHttpHelper.token = authToken;
    };
    return process(new BaseHttpHelper<UserDataModel>(this.http, this.router, '/auth', model).post(), listener, f);
  }

  deAuth(listener: LoadingListener): Observable<any> {
    let f = () => {
      localStorage.clear();
      BaseHttpHelper.token = null;
      this.router.navigate(['/login']);
    };
    return process(new BaseHttpHelper<any>(this.http, this.router, '/auth').delete(), listener, f);
  }
}
