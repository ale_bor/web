import {Injectable} from "@angular/core";
import {Point, PointDataModel} from "../models/point";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {BaseHttpHelper} from "../helpers/base-http.helper";
import {Router} from "@angular/router";
import {handleData, LoadingListener, process} from "../helpers/response.helper";

@Injectable()
export class PointsService {

  constructor(private http: Http,
              private router: Router) {
  }

  points(listener: LoadingListener): Observable<Point[]> {
    return process(new BaseHttpHelper<PointDataModel[]>(this.http, this.router, "/points"), listener)
      .map(handleData)
      .flatMap((models: PointDataModel[]) => Observable.from(models))
      .map((model: PointDataModel) => Point.fromModel(model))
      .toArray();
  }

  point(listener: LoadingListener, id: number): Observable<Point> {
    return process(new BaseHttpHelper<PointDataModel>(this.http, this.router, `/points/${id}`), listener)
      .map(handleData)
      .map((model: PointDataModel) => Point.fromModel(model));
  }

  create(listener: LoadingListener, point: Point): Observable<Point> {
    return process(new BaseHttpHelper<PointDataModel>(this.http, this.router, `/points`, point.model).put(), listener)
      .map(handleData)
      .map((model: PointDataModel) => Point.fromModel(model));
  }

  update(listener: LoadingListener, point: Point): Observable<Point> {
    return process(new BaseHttpHelper<PointDataModel>(this.http, this.router, `/points/${point.id}`, point.model).put(), listener)
      .map(handleData)
      .map((model: PointDataModel) => Point.fromModel(model));
  }

  deletePoint(listener: LoadingListener, id: number): Observable<any> {
    return process(new BaseHttpHelper<any>(this.http, this.router, `/points/${id}/delete`), listener)
      .map(handleData);
  }
}
