import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {User} from "../models/user";
import {UserPoint} from "../models/point";
import {dateWithoutTime} from "../helpers/date.helper";

@Injectable()
export class NavbarService {
  private subjectAction: BehaviorSubject<Action> = new BehaviorSubject(null);
  private subjectUser: BehaviorSubject<User> = new BehaviorSubject(null);
  private subjectSimpePoint: BehaviorSubject<UserPoint> = new BehaviorSubject(null);
  private subjectDate: BehaviorSubject<Date> = new BehaviorSubject(dateWithoutTime());

  private _user: User;
  private _selectedPoint: UserPoint;

  constructor() {
    let ts = localStorage.getItem("ts");
    let date;
    if (ts) {
      date = new Date(Number(ts));
    } else {
      date = dateWithoutTime();
      localStorage.setItem("ts", String(date.getTime()));
    }
    this.subjectDate.next(date);
  }

  get actions(): Observable<Action> {
    return this.subjectAction.asObservable().filter(a => a !== null);
  }

  set action(action: Action) {
    this.subjectAction.next(action);
  }

  getUser(): Observable<User> {
    return this.subjectUser.asObservable().filter(a => a !== null);
  }

  setUser(user: User) {
    this._user = user;
    this.subjectAction.next(new Action(ActionType.change, user, this._selectedPoint));
    this.subjectUser.next(user);
  }

  getSelectedPoint(): Observable<UserPoint> {
    return this.subjectSimpePoint.asObservable().filter(a => a !== null);
  }

  setSelectedPoint(simplePoint: UserPoint) {
    this._selectedPoint = simplePoint;
    this.subjectAction.next(new Action(ActionType.change, this._user, simplePoint));
    this.subjectSimpePoint.next(simplePoint);
  }

  getDate(): Observable<Date> {
    return this.subjectDate.asObservable();
  }

  setDate(date: Date) {
    localStorage.setItem("ts", String(date.getTime()));
    this.subjectDate.next(date);
  }
}

export class Action {
  type: ActionType;
  user?: User;
  point?: UserPoint;

  constructor(type: ActionType, user?: User, point?: UserPoint) {
    this.type = type;
    this.user = user;
    this.point = point;
  }
}

export enum ActionType {
  reload, deAuth, auth, none, change
}
