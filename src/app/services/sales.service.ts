import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Http} from "@angular/http";
import {Router} from "@angular/router";
import {BaseHttpHelper} from "../helpers/base-http.helper";
import {handleData, LoadingListener, process} from "../helpers/response.helper";
import {Sale, SaleModel, SaleType} from "../models/sale/Sale";
import {saleFromModel} from "../helpers/sales.helper";
import {PointDigest, PointDigestModel} from "../models/sale/digest/PointDigest";
import {Day, DayModel} from "../models/sale/digest/Day";
import {dateToPHP, onlyDateToPHP} from "../helpers/date-formatter.helper";

@Injectable()
export class SalesService {

  constructor(private http: Http,
              private router: Router) {
  }

  digest(listener: LoadingListener, startDate: Date, endDate: Date): Observable<PointDigest[]> {
    const uri = `/days/digest/${onlyDateToPHP(startDate)}/${onlyDateToPHP(endDate)}`;
    return process(new BaseHttpHelper<PointDigestModel[]>(this.http, this.router, uri), listener)
      .map(handleData)
      .flatMap((models: PointDigestModel[]) => Observable.from(models))
      .map((model: PointDigestModel) => PointDigest.fromModel(model))
      .toArray();
  }

  sales(listener: LoadingListener, pointId: number, type: SaleType, startDate: Date, endDate: Date): Observable<Sale[]> {
    const uri = `/sales/${pointId}/${SaleType[type]}/${onlyDateToPHP(startDate)}/${dateToPHP(endDate)}`;
    return process(new BaseHttpHelper<SaleModel[]>(this.http, this.router, uri), listener)
      .map(handleData)
      .flatMap(models => Observable.from(models))
      .map((model: SaleModel) => saleFromModel(model))
      .toArray();
  }

  createSale(listener: LoadingListener, sale: Sale): Observable<Sale> {
    const saleModel = sale.model;
    return process(new BaseHttpHelper<SaleModel>(this.http, this.router, `/sales/${saleModel.type}`, saleModel).put(), listener)
      .map(handleData)
      .map((model: SaleModel) => saleFromModel(model));
  }

  deleteSale(listener: LoadingListener, id: number) {
    return process(new BaseHttpHelper<any>(this.http, this.router, `/sales/${id}`).delete(), listener);
  }

  getDay(listener: LoadingListener, pointId: number, date: Date): Observable<Day> {
    const path = `/days/${pointId}/${onlyDateToPHP(date)}/status`;
    return process(new BaseHttpHelper<DayModel>(this.http, this.router, path), listener)
      .map(handleData)
      .map((s: DayModel) => Day.fromModel(s));
  }

  updateDay(listener: LoadingListener, day: Day): Observable<Day> {
    return process(new BaseHttpHelper<DayModel>(this.http, this.router, `/days/status`, {day: day.model}).put(), listener)
      .map(handleData)
      .map((s: DayModel) => Day.fromModel(s));
  }

  deleteDay(listener: LoadingListener, pointId: number, date: Date): Observable<any> {
    const url = `/days/${pointId}/${onlyDateToPHP(date)}`;
    return process(new BaseHttpHelper<any>(this.http, this.router, url).delete(), listener);
  }
}

export enum SalesFormatType {
  //noinspection JSUnusedGlobalSymbols
  CSV, XLS, XLSX, HTML, PDF
}
