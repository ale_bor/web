import {Injectable} from "@angular/core";
import {handleData, LoadingListener, process} from "../helpers/response.helper";
import {Observable} from "rxjs";
import {Log, LogDataModel} from "../models/Log";
import {BaseHttpHelper} from "../helpers/base-http.helper";
import {Http} from "@angular/http";
import {Router} from "@angular/router";
import {onlyDateToPHP} from "../helpers/date-formatter.helper";

@Injectable()
export class LogService {

  constructor(private http: Http,
              private router: Router) {
  }

  load(loadingListener: LoadingListener, startDate: Date, endDate: Date): Observable<Log[]> {
    let path = `/logs/${onlyDateToPHP(startDate)}/${onlyDateToPHP(endDate)}`;
    return process(new BaseHttpHelper<LogDataModel[]>(this.http, this.router, path), loadingListener)
      .map(handleData)
      .flatMap((models: LogDataModel[]) => Observable.from(models))
      .map((model: LogDataModel) => Log.fromModel(model))
      .toArray();
  }
}
