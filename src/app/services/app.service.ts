import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {LoadingListener} from "../helpers/response.helper";

@Injectable()
export class AppService implements LoadingListener {
  private inLoadingCount: number = 0;

  private behaviorSubjectAlertAction: BehaviorSubject<AlertAction> = new BehaviorSubject(null);
  private behaviorSubjectInLoading: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private behaviorSubjectConfirmation: BehaviorSubject<ConfirmAction> = new BehaviorSubject(null);

  alert(action: AlertAction) {
    this.behaviorSubjectAlertAction.next(action);
  }

  error(text: string) {
    this.alert(new AlertAction("Ошибка", text, AlertType.ERROR));
  }

  confirm(action: ConfirmAction): Observable<boolean> {
    this.behaviorSubjectConfirmation.next(action);
    return action.confirm;
  }

  get alertActions(): Observable<AlertAction> {
    return this.behaviorSubjectAlertAction.asObservable().filter(a => a !== null);
  }

  get loading(): Observable<boolean> {
    return this.behaviorSubjectInLoading.asObservable();
  }

  get confirmation(): Observable<ConfirmAction> {
    return this.behaviorSubjectConfirmation.asObservable().filter(a => a !== null);
  }

  private set loadingStatus(inLoading: boolean) {
    this.behaviorSubjectInLoading.next(inLoading);
  }

  startLoading() {
    this.inLoadingCount++;
    this.loadingStatus = true;
  }

  stopLoading(force?: boolean): void {
    if (force) {
      this.inLoadingCount = 0;
    } else {
      if (this.inLoadingCount > 0) {
        this.inLoadingCount--;
      } else {
        this.inLoadingCount = 0;
      }
    }
    if (this.inLoadingCount === 0) {
      this.loadingStatus = false;
    }
  }
}

export enum AlertType {
  DEFAULT, ERROR, WARNING, SUCCESS
}

export class AlertAction {
  title: string;
  content: string;
  type: AlertType;

  constructor(title: string, content: string, type: AlertType = AlertType.DEFAULT) {
    this.title = title;
    this.content = content;
    this.type = type;
  }
}

export class ConfirmAction extends AlertAction {
  accept: string;

  private _confirm: Subject<boolean> = new Subject();

  constructor(title: string, content: string, acceptText: string, type: AlertType = AlertType.DEFAULT) {
    super(title, content, type);
    this.accept = acceptText;
  }

  get confirm(): Observable<boolean> {
    return this._confirm.asObservable().filter(a => a !== null);
  }

  public result(value: boolean) {
    this._confirm.next(value);
    this._confirm.complete();
  }
}
