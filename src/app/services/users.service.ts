import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {User, UserDataModel} from "../models/user";
import {BaseHttpHelper} from "../helpers/base-http.helper";
import {handleData, LoadingListener, process} from "../helpers/response.helper";

@Injectable()
export class UsersService {
  constructor(private http: Http, private router: Router) {
  }

  create(listener: LoadingListener, user: User): Observable<User> {
    return process(new BaseHttpHelper<UserDataModel>(this.http, this.router, `/users`, user.model).put(), listener)
      .map(handleData)
      .map((model: UserDataModel) => User.fromModel(model));
  }

  users(listener: LoadingListener): Observable<User[]> {
    return process(new BaseHttpHelper<UserDataModel[]>(this.http, this.router, `/users`), listener)
      .map(handleData)
      .flatMap((models: UserDataModel[]) => Observable.from(models))
      .map((model: UserDataModel) => User.fromModel(model))
      .toArray();
  }

  user(listener: LoadingListener, id: number): Observable<User> {
    return process(new BaseHttpHelper<UserDataModel>(this.http, this.router, `/users/${id}`), listener)
      .map(handleData)
      .map((model: UserDataModel) => User.fromModel(model));
  }

  update(listener: LoadingListener, user: User): Observable<User> {
    return process(new BaseHttpHelper<UserDataModel>(this.http, this.router, `/users/${user.id}`, user.model).put(), listener)
      .map(handleData)
      .map((model: UserDataModel) => User.fromModel(model));
  }

  deleteUser(listener: LoadingListener, id: number): Observable<any> {
    return process(new BaseHttpHelper<any>(this.http, this.router, `/users/${id}/delete`), listener)
      .map(handleData);
  }

  currentUser(listener: LoadingListener): Observable<User> {
    return this.user(listener, 0);
  }
}
