import {Component, OnInit} from "@angular/core";
import {Point} from "../../../models/point";
import {PointsService} from "../../../services/points.service";
import {AppService} from "../../../services/app.service";

@Component({
  selector: 'app-points-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [PointsService]
})
export class PointsListComponent implements OnInit {
  points: Point[];

  constructor(private pointsService: PointsService,
              private appService: AppService) {
  }

  ngOnInit(): void {
    this.pointsService.points(this.appService)
      .subscribe((points: Point[]) => this.points = points);
  }
}
