import {Component, OnDestroy, OnInit} from "@angular/core";
import {Location} from "@angular/common";
import {ActivatedRoute, Router} from "@angular/router";
import {Point, PointCheckType, PointCurrency, PointStatus} from "../../../models/point";
import {PointsService} from "../../../services/points.service";
import {getEnumKeys} from "../../../helpers/enum.helper";
import {routeNumberValue} from "../../../helpers/route.helper";
import {AlertAction, AlertType, AppService} from "../../../services/app.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-point-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class PointEditComponent implements OnInit, OnDestroy {
  pointId: number;
  point: Point;
  pointStatuses: number[];
  pointCurrencies: number[];
  pointCheckTypes: number[];
  inLoading: boolean;
  error: string;
  private loadingSubscription: Subscription;

  constructor(private router: Router,
              private pointsService: PointsService,
              private route: ActivatedRoute,
              private appService: AppService,
              private _location: Location) {
    this.pointStatuses = getEnumKeys(PointStatus);
    this.pointCurrencies = getEnumKeys(PointCurrency);
    this.pointCheckTypes = getEnumKeys(PointCheckType);
    this.pointId = routeNumberValue(route, 'id');
  }

  ngOnInit() {
    if (this.pointId) {
      this.pointsService.point(this.appService, this.pointId)
        .subscribe((point: Point) => this.point = point);
    } else {
      this.point = new Point(0, "", 0, "", "", PointStatus.NEW, PointCurrency.RUB, PointCheckType.FULL);
    }

    this.loadingSubscription = this.appService.loading.subscribe((inLoading: boolean) => this.inLoading = inLoading);
  }

  ngOnDestroy(): void {
    if (this.loadingSubscription) {
      this.loadingSubscription.unsubscribe();
    }
  }

  onSubmit(): void {
    if (this.pointId) {
      this.pointsService.update(this.appService, this.point)
        .subscribe((point: Point) => {
          this.point = point;
          this.appService.alert(new AlertAction("Сохранено", "Данные объекта успешно сохранены"));
        }, error => this.appService.error(error));
    } else {
      this.pointsService.create(this.appService, this.point)
        .subscribe((point: Point) => {
          this.router.navigate(['/points/edit', point.id]);
          this.appService.alert(new AlertAction("Сохранено", "Новый объект успешно добавлен"));
        }, error => this.appService.error(error));
    }
  }

  deletePoint() {
    this.pointsService.deletePoint(this.appService, this.point.id).subscribe(() => {
        this.router.navigate(['/points']);
        this.appService.alert(new AlertAction("Успешно", "Объект удалён", AlertType.DEFAULT));
      },
      error => this.appService.alert(new AlertAction("Ошибка", error, AlertType.ERROR)));
  }

  isNotNewPointStatus(status: PointStatus): boolean {
    return status == PointStatus.NEW;
  }

  goBack() {
    this._location.back();
  }

}
