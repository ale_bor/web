import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {Group} from "../../../models/group";
import {GroupsService} from "../../../services/groups.service";
import {NavbarService} from "../../../services/navbar.service";
import {routeNumberValue} from "../../../helpers/route.helper";
import {User} from "../../../models/user";
import {AppService} from "../../../services/app.service";

@Component({
  selector: 'app-edit-group',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class GroupEditComponent implements OnInit {
  groupId: number;
  group: Group;
  inProgress: boolean;
  error: string;

  constructor(private router: Router,
              private groupsService: GroupsService,
              private navbarService: NavbarService,
              private route: ActivatedRoute,
              private appService: AppService) {
    this.groupId = routeNumberValue(route, 'id');
    this.inProgress = false;
  }

  ngOnInit() {
    if (this.groupId) {
      this.groupsService.group(this.appService, this.groupId)
      //TODO: Обработка ошибок
        .subscribe((group: Group) => this.group = group);
    } else {
      this.group = new Group();
      this.navbarService.getUser().subscribe((user: User) => this.group.owner = user.id);
    }
  }

  onSubmit() {
    this.inProgress = true;
    this.error = null;
    if (this.groupId) {
      this.groupsService.update(this.appService, this.group)
        .subscribe((user: Group) => this.group = user,
          error => this.error = error,
          () => this.inProgress = false);
    } else {
      this.groupsService.create(this.appService, this.group)
        .subscribe((user: Group) => this.router.navigate(['/groups/edit/', user.id]),
          error => this.error = error,
          () => this.inProgress = false);
    }
  }

}
