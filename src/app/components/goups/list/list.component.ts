import {Component, OnInit} from "@angular/core";
import {GroupsService} from "../../../services/groups.service";
import {Group} from "../../../models/group";
import {AppService} from "../../../services/app.service";

@Component({
  selector: 'app-groups-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class GroupsListComponent implements OnInit {
  groups: Group[];

  constructor(private groupsService: GroupsService,
              private appService: AppService) {
  }

  ngOnInit() {
    this.groupsService.groups(this.appService).subscribe((groups: Group[]) => this.groups = groups);
  }

}
