import {Component, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {User} from "../../models/user";
import {NavbarService} from "../../services/navbar.service";
import {compareWithoutTime, dateWithoutTime} from "../../helpers/date.helper";
import {UserPoint} from "../../models/point";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  user: User;
  point: UserPoint;
  date: Date = dateWithoutTime();
  private userSubscription: Subscription;
  private pointSubscription: Subscription;
  private dateSubscription: Subscription;

  constructor(private navbarService: NavbarService) {
  }

  ngOnInit() {
    this.userSubscription = this.navbarService.getUser().subscribe((user: User) => this.user = user);
    this.pointSubscription = this.navbarService.getSelectedPoint().subscribe((point: UserPoint) => this.point = point);
    this.dateSubscription = this.navbarService.getDate()
      .filter((date) => compareWithoutTime(date, this.date))
      .subscribe((date: Date) => this.date = date);
  }

  ngOnDestroy(): void {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
    if (this.pointSubscription) {
      this.pointSubscription.unsubscribe();
    }
    if (this.dateSubscription) {
      this.dateSubscription.unsubscribe();
    }
  }

  get timestamp(): number {
    return this.date.getTime();
  }

}
