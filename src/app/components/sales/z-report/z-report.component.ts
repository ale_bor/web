import {Component} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {BasicActionsComponent} from "../../../helpers/basic-actions-component";
import {NavbarService} from "../../../services/navbar.service";
import {AppService} from "../../../services/app.service";
import {SalesService} from "../../../services/sales.service";
import {ZReport} from "../../../models/sale/ZReport";
import {SaleType} from "../../../models/sale/Sale";
import {compareWithoutTime, datePlus, dateWithoutTime, getMonday} from "../../../helpers/date.helper";
import {DayStatus} from "../../../models/sale/digest/DayStatus";
import {getEnumKeys} from "../../../helpers/enum.helper";

@Component({
  selector: 'app-z-report-creator',
  templateUrl: './z-report.component.html',
  styleUrls: ['./z-report.component.scss']
})
export class ZReportEditComponent extends BasicActionsComponent {
  reports: ZReport[] = [];

  constructor(protected router: Router,
              protected navbarService: NavbarService,
              protected route: ActivatedRoute,
              private salesService: SalesService,
              private appService: AppService) {
    super(router, navbarService, route);
    this.dayStatuses = getEnumKeys(DayStatus, [DayStatus.SALE, DayStatus.CLOSED, DayStatus.E_WORK, DayStatus.NOSALE]);
  }

  protected load() {
    let monday = getMonday(dateWithoutTime(this.startDate));
    //noinspection TypeScriptValidateTypes
    this.salesService.sales(
      this.appService,
      this.point.point,
      SaleType.ZREPORT,
      monday,
      datePlus(6, monday)
    )
      .flatMap((reports: ZReport[]) => Observable.from(reports))
      .do((report: ZReport) => report.isSaved = true)
      .toArray()
      .subscribe((reports: ZReport[]) => {
        this.reports = reports;
        Observable.from(this.reports)
          .map((report: ZReport) => report.date.getDay())
          .toArray()
          .subscribe((days: number[]) => {
            let monday = getMonday(this.startDate);
            let mondayDayNumber: number = monday.getDate();

            let year = monday.getFullYear();
            let month = monday.getMonth();

            Observable.range(1, 7)
              .filter(day => days.indexOf(day) === -1)
              .subscribe((day: number) => {
                let currentDay = mondayDayNumber + Number(day) - 1;
                let reportDate: Date = new Date(year, month, currentDay);
                this.reports.push(new ZReport(
                  null,
                  reportDate,
                  -1,
                  this.user.id,
                  this.point.id,
                  null,
                  null
                ));
              });
          });
      });
  }

  save(): void {
    Observable.from(this.reports)
      .filter((report: ZReport) => !report.isSaved)
      .filter((report: ZReport) => report.isValid || (!report.isValid && report.count !== null && report.sum !== null))
      .do((report: ZReport) => {
        if (report.isValid) {
          report.sum = report.count = 0;
        }
        report.pointId = this.point.point;
        report.userId = this.user.id;
      })
      .subscribe((report: ZReport) => {
        this.salesService.createSale(this.appService, report)
          .subscribe((report: ZReport) => {
            Observable.from(this.reports)
              .filter((r: ZReport) => compareWithoutTime(r.date, report.date))
              .subscribe((r: ZReport) => r.isSaved = true);
          });
      });
  }

  get sortedReports(): ZReport[] {
    return this.reports.sort((r1: ZReport, r2: ZReport) => r1.date.getTime() - r2.date.getTime());
  }

  //noinspection JSMethodCanBeStatic
  isDayAfterToday(date: Date): boolean {
    return dateWithoutTime(date) > dateWithoutTime();
  }

  get sum(): number {
    let sum: number = 0;
    Observable.from(this.sortedReports)
      .filter((report: ZReport) => Boolean(report.sum))
      .subscribe((report: ZReport) => sum += Number(report.sum));
    return sum;
  }

  //noinspection JSUnusedGlobalSymbols
  get checksSum(): number {
    let checksSum: number = 0;
    Observable.from(this.sortedReports)
      .filter((report: ZReport) => Boolean(report.count))
      .subscribe((report: ZReport) => checksSum += Number(report.count));
    return checksSum;
  }

  get startDate(): Date {
    return this.date;
  }

  get endDate(): Date {
    return this.date;
  }
}
