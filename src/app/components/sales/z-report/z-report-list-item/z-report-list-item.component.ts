import {Component, EventEmitter, Input, Output} from "@angular/core";
import {ZReport} from "../../../../models/sale/ZReport";
import {DayStatus} from "../../../../models/sale/digest/DayStatus";

@Component({
  selector: '[app-z-report-list-item]',
  templateUrl: './z-report-list-item.component.html'
})
export class ZReportListItemComponent {

  @Input() report: ZReport;
  @Input() isDayAfterToday: boolean;
  @Input() dayStatuses: DayStatus[];
  @Output() save: EventEmitter<ZReport> = new EventEmitter<ZReport>(true);

}
