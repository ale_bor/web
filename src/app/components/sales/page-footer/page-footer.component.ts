import {AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {Location} from "@angular/common";
import {SaleType} from "../../../models/sale/Sale";
import {Day} from "../../../models/sale/digest/Day";
import {DayStatus} from "../../../models/sale/digest/DayStatus";

@Component({
  selector: 'app-sale-page-footer',
  templateUrl: './page-footer.component.html'
})
export class SalePageFooterComponent implements AfterViewInit {
  @Input() day: Day;
  @Input() dayStatuses: DayStatus[] = [];

  @Input() startDate: Date;
  @Input() endDate: Date;

  @Input() pointId: number;
  @Input() type: SaleType;
  @Input() isAdmin: boolean;
  @Input() write: boolean;
  @Input() isHandleEnabled: boolean;
  @Input() isBlocked: boolean;
  @Input() hasUnsaved: boolean;

  @Output() save: EventEmitter<any> = new EventEmitter<any>();
  @Output() upload: EventEmitter<any> = new EventEmitter<any>();
  @Output() unlock: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('saveModal') saveModal: ElementRef;

  constructor(private _location: Location) {
  }

  ngAfterViewInit(): void {
    $('[tooltip]').tooltip();
  }

  onSave() {
    $(this.saveModal.nativeElement).modal('hide');
    this.save.emit();
  }

  goBack() {
    this._location.back();
  }
}
