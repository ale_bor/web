import {AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";
import {Http} from "@angular/http";
import {BaseHttpHelper} from "../../../helpers/base-http.helper";
import {handleData} from "../../../helpers/response.helper";
import {Router} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
import {dateFromPHP} from "../../../helpers/date-formatter.helper";

interface ErrorDateModel {
  date: string;
  error: string;
}

class ErrorDate {
  date: Date;
  error: string;

  constructor(date: Date, error: string) {
    this.date = date;
    this.error = error;
  }

  static fromModel(model: ErrorDateModel): ErrorDate {
    return new ErrorDate(
      dateFromPHP(model.date),
      model.error
    );
  }
}

class GroupedError {
  error: string = null;
  dates: Date[] = [];
}

export interface UploadV2DataModel {
  turnSize: string;
  hash: string;
  session: string;
}

export class UploadV2Data {
  turnSize: number;
  hash: string;
  session: string;

  constructor(turnSize: number, hash: string, session: string) {
    this.turnSize = turnSize;
    this.hash = hash;
    this.session = session;
  }

  static fromModel(model: UploadV2DataModel): UploadV2Data {
    return new UploadV2Data(
      Number(model.turnSize),
      model.hash,
      model.session
    );
  }
}

export interface UploadInProgressDataModel {
  turnPosition: string;
}

export class UploadInProgressData {
  turnPosition: number;

  constructor(turnPosition: number) {
    this.turnPosition = turnPosition;
  }

  static fromModel(model: UploadInProgressDataModel): UploadInProgressData {
    return new UploadInProgressData(Number(model.turnPosition));
  }
}

export interface UploadDataModel {
  dates: string[];
  errorDates: ErrorDateModel[];
}

export class UploadData {
  dates: Date[];
  errorDates: ErrorDate[];

  constructor(dates: Date[], errorDates: ErrorDate[]) {
    this.dates = dates;
    this.errorDates = errorDates;
  }

  static fromModel(model: UploadDataModel): UploadData {
    return new UploadData(
      model.dates ? model.dates.map((ts: string) => dateFromPHP(ts)) : [],
      model.errorDates ? model.errorDates.map((model: ErrorDateModel) => ErrorDate.fromModel(model)) : []
    );
  }
}

@Component({
  selector: 'app-upload-modal',
  templateUrl: './upload-modal.component.html',
  styleUrls: ['upload-modal.component.scss']
})
export class UploadModalComponent implements AfterViewInit {
  @ViewChild('progressBar') progressBarElement: ElementRef;
  @ViewChild('modal') modalElement: ElementRef;

  @Input() pointId: number;
  @Input() templateEXCELUrl: string;
  @Output() onSuccess: EventEmitter<any> = new EventEmitter<any>();
  @Output() beforeUpload: EventEmitter<any> = new EventEmitter<any>();
  hasBaseDropZoneOver: boolean = false;

  inProgress: boolean = false;

  turnSize: number = 0;
  private _turnStep: number = 0;

  success: string = null;
  error: string = null;
  dates: Date[] = [];
  errorDates: GroupedError[] = [];
  private progressSubscription: Subscription;

  constructor(private http: Http,
              private router: Router) {
  }

  ngAfterViewInit(): void {
    $('[tooltip]').tooltip();
    $(this.modalElement.nativeElement)
      .on('show.bs.modal', () => {
        this.show();
      })
      .on('hide.bs.modal', () => {
        this.close();
      });
  }

  show(): void {
    this.inProgress = false;
    this.clearInfo();
  }

  clearInfo() {
    this.turnSize = 0;
    this.turnStep = 0;
    this.success = null;
    this.error = null;
    this.errorDates = [];
    this.dates = [];
  }

  handleUpload(data): void {
    this.clearInfo();

    if (data && data.response) {
      data = JSON.parse(data.response);
      if (data.error) {
        this.error = data.error;
        this.inProgress = false;
        return;
      }

      Observable.from([data.data])
        .map((model: UploadV2DataModel) => UploadV2Data.fromModel(model))
        .subscribe((data: UploadV2Data) => {
          const hash = data.hash;
          const session = data.session;
          if (!this.inProgress) {
            this.cancel(hash, session);
            return;
          }
          const turnSize = data.turnSize;
          this.turnSize = turnSize;
          this.turnStep = 0;
          this.progressSubscription = new BaseHttpHelper<UploadInProgressDataModel>(this.http, this.router, `/sales/upload/v2/${session}/${hash}`)
            .patch()
            .build()
            .map(handleData)
            .map((model: UploadInProgressDataModel) => UploadInProgressData.fromModel(model))
            .repeat(turnSize)
            .subscribe((data: UploadInProgressData) => {
              this.turnStep = data.turnPosition;
              if (this.inProgress && data.turnPosition == turnSize) {
                new BaseHttpHelper<UploadDataModel>(this.http, this.router, `/sales/upload/v2/${session}/${hash}`)
                  .patch()
                  .build()
                  .map(handleData)
                  .map((model: UploadDataModel) => UploadData.fromModel(model))
                  .subscribe((data: UploadData) => {
                    this.dates = data.dates;

                    Observable.from(data.errorDates)
                      .groupBy((x: ErrorDate) => x.error)
                      .flatMap(group => {
                        return group.reduce((groupedError: GroupedError, errorDate: ErrorDate) => {
                          groupedError.error = errorDate.error;
                          groupedError.dates.push(errorDate.date);
                          return groupedError;
                        }, new GroupedError())
                      })
                      .toArray()
                      .subscribe((errorDates: GroupedError[]) => this.errorDates = errorDates);

                    if (data.dates.length > 0) {
                      this.success = data.errorDates.length == 0 ? "Данные загружены" : "Данные загружены частично";
                    } else {
                      this.error = "Ошибка при загрузке данных";
                    }
                    this.onSuccess.emit();
                    this.inProgress = false;
                  }, () => {
                    this.error = "Ошибка при загрузке данных";
                    this.inProgress = false;
                  });
              } else if (!this.inProgress) {
                this.cancel(hash, session);
              }
            }, () => {
              this.error = "Ошибка при загрузке данных";
              this.inProgress = false;
            });
        });
    }
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  beforeUploadF(uploadingFile): void {
    this.hasBaseDropZoneOver = false;
    this.beforeUpload.emit(uploadingFile);

    this.inProgress = true;
  }

  //noinspection JSUnusedGlobalSymbols
  get options(): any {
    return {
      url: `${environment.apiUrl}/sales/upload/v2/${this.pointId}`,
      data: {token: localStorage.getItem('token')}
    };
  }

  get turnStep(): number {
    return this._turnStep;
  }

  set turnStep(value: number) {
    this._turnStep = value;
    if (this.turnStep < this.turnSize && this.turnSize > 0) {

      let percent = (this.turnStep + 1) * 100 / this.turnSize;

      if (this.progressBarElement && this.progressBarElement.nativeElement) {
        $(this.progressBarElement.nativeElement)
          .attr('aria-valuenow', this.turnStep)
          .attr('aria-valuemax', this.turnSize)
          .width(percent + '%');
      }
    }
  }

  close() {
    this.inProgress = false;
  }

  private cancel(hash: string, session: string) {
    if (this.progressSubscription) {
      this.progressSubscription.unsubscribe();
    }
    new BaseHttpHelper<UploadDataModel>(this.http, this.router, `/sales/upload/v2/${session}/${hash}`)
      .delete()
      .build()
      .subscribe();
  }
}
