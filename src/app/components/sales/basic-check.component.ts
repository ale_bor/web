import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {BasicActionsComponent} from "../../helpers/basic-actions-component";
import {NavbarService} from "../../services/navbar.service";
import {AlertAction, AlertType, AppService, ConfirmAction} from "../../services/app.service";
import {SalesService} from "../../services/sales.service";
import {OperationType, Sale, SaleType} from "../../models/sale/Sale";
import {compareWithoutSeconds, dateWithoutTime, timestampPlus} from "../../helpers/date.helper";
import {getEnumKeys} from "../../helpers/enum.helper";
import {Full} from "../../models/sale/Full";
import {process} from "../../helpers/response.helper";
import {Day} from "../../models/sale/digest/Day";
import {DayStatus} from "../../models/sale/digest/DayStatus";

export abstract class BasicSaleComponent extends BasicActionsComponent {
  public readonly templateEXCELUrl: string = '';

  sales: Full[] = [];
  operationTypes: number[];
  day: Day = new Day(0, new Date(), DayStatus.NODATA, false, 0);
  importType: SaleType = SaleType.FULL;

  public inLoading: boolean;

  protected constructor(protected navbarService: NavbarService,
                        protected salesService: SalesService,
                        protected route: ActivatedRoute,
                        protected router: Router,
                        private appService: AppService) {
    super(router, navbarService, route);
    this.operationTypes = getEnumKeys(OperationType, [OperationType.SALE, OperationType.RETURN]);
  }

  protected load(): void {
    this.salesService.getDay(this.appService, this.point.point, this.date)
      .subscribe((day: Day) => {
        this.day = day;

        let dwf = dateWithoutTime(this._date);

        //noinspection TypeScriptValidateTypes
        this.salesService.sales(
          this.appService,
          this.point.point,
          SaleType.FULL,
          dwf,
          new Date(timestampPlus(1, dwf) - 1000)
        )
          .flatMap((reports: Full[]) => Observable.from(reports))
          .do((report: Full) => report.isSaved = true)
          .toArray()
          .subscribe((sales: Full[]) => {
            this.sales = sales;

            if (this.day.isStatusNoData) {
              this.day.status = sales.length > 0 ? DayStatus.SALE : DayStatus.NOSALE;
            }

            let index: number = -1;
            this.sales.forEach((sale: Full) => sale.order = index++);
          });
      });
  }

  /**
   * Закрытие дня
   */
  public lock(): void {
    if (this.day.isStatusNoData) {
      this.updateStatus();
    }
    let uploads: Observable<Sale> | boolean = this.upload();
    if (uploads) {
      this.day.closed = true;
      (<Observable<Sale>>uploads).subscribe(() => this.saveDay("День успешно закрыт"));
    } else {
      this.appService.error("Заполнены не все поля");
    }
  }

  private updateStatus() {
    if (this.sales.length > 0) {
      this.day.status = DayStatus.SALE;
    }
    if (this.day.isStatusNoData || this.sales.length == 0) {
      this.day.status = DayStatus.NOSALE;
    }
  }

  public unlock(): void {
    this.day.closed = false;
    this.day.status = DayStatus.NODATA;
    this.saveDay("День успешно открыт");
  }

  private saveDay(onSuccessText?: string): Observable<Day> {
    return process(this.salesService.updateDay(this.appService, this.day),
      null,
      (day: Day) => {
        this.day = day;
        this.appService.alert(new AlertAction("Успешно", onSuccessText ? onSuccessText : "Данные успешно сохранены", AlertType.SUCCESS));
      });
  }

  /**
   * Отправка данных
   */
  public upload(): Observable<Sale> | boolean {
    let forSave: Full[] = this.sortedSales.filter((sale: Full) => !sale.isSaved);
    if (forSave.length > 0) {
      if (forSave.filter((sale: Full) => !sale.isValid).length > 0) {
        return false;
      }
      return process<Full>(Observable.from(forSave)
          .do((sale: Full) => {
            sale.pointId = this.point.point;
            sale.userId = this.user.id;
          }),
        null,
        (sale: Full) => {
          this.salesService.createSale(this.appService, sale)
            .subscribe((savedSale: Full) => {
              Observable.from(this.sales)
                .filter((sale: Full) => !sale.isSaved)
                .filter((sale: Full) => compareWithoutSeconds(sale.date, savedSale.date) && sale.number == savedSale.number && sale.till == savedSale.till && sale.operationType == savedSale.operationType)
                .subscribe((sale: Full) => {
                  sale.id = savedSale.id;
                  sale.isSaved = true;
                  this.onSaveSuccess(sale, savedSale);
                  this.day.status = DayStatus.SALE;
                });
            });
        });
    } else {
      return Observable.from([null]);
    }
  }

  //noinspection ReservedWordAsName
  public delete(sale: Full): void {
    if (sale.isSaved) {
      if (confirm("Удалить данный чек?")) {
        this.salesService.deleteSale(this.appService, sale.id)
          .subscribe(() => {
            this.sales.splice(this.sales.indexOf(sale), 1);
            this.updateStatus();
          });
      }
    } else {
      this.sales.splice(this.sales.indexOf(sale), 1);
      this.updateStatus();
    }
  }

  public purge(): void {
    if (!this.dayClosed) {
      if (this.sales && this.sales.length > 0) {
        this.appService.confirm(new ConfirmAction("Внимание", "Очистить данные за день?", "Подтвердить"))
          .filter(accept => accept)
          .subscribe(() => {
            this.salesService.deleteDay(this.appService, this.pointId, this.date).subscribe(() => {
              this.preLoad();
              this.updateStatus();
              this.appService.alert(new AlertAction("Очищено", "Очищено", AlertType.SUCCESS));
            }, error => this.appService.error(error));
          });
      } else {
        this.appService.alert(new AlertAction("Ошибка", "День уже пуст", AlertType.WARNING));
      }
    }
  }

  protected abstract onSaveSuccess(originalSale: Sale, savedSale: Full): void;

  public get sortedSales(): Full[] {
    return this.sales
      .sort((sale1: Full, sale2: Full) => {
        if (sale1.isSaved && !sale2.isSaved) {
          return 1;
        } else if (!sale1.isSaved && sale2.isSaved) {
          return -1;
        } else if (sale1.isSaved && sale2.isSaved) {
          return sale1.id - sale2.id;
        } else {
          return sale2.order - sale1.order;
        }
      });
  }

  public abstract get sum(): number;

  public abstract get sumOfReturns(): number;

  public abstract get checksSum(): number;

  public abstract get checksSumOfReturns(): number;

  //noinspection JSUnusedGlobalSymbols
  public add(): void {
    let sale = this.newSale;
    sale.order = this.sales.length + 1;
    this.sales.push(sale);

    this.toggleVisibility(sale);
    this.updateStatus();
  }

  protected abstract get newSale(): Full;

  public nextSaleNumber(): number {
    let result: number = 0;

    Observable.from(this.sales)
      .map((full: Full) => parseFloat(full.number))
      .max()
      .subscribe(x => result = x);

    return result + 1;
  }

  public get startDate(): Date {
    return this.date;
  }

  public get endDate(): Date {
    return this.date;
  }

  //noinspection JSUnusedLocalSymbols
  public onUploadSuccess(sales: Full[]): void {
    this.load();
  }

  toggleVisibility(sale: Full) {
    let isOpen = sale.isOpen;

    if (!isOpen) {
      this.sales.forEach(sale => sale.isOpen = false);
    }

    sale.isOpen = !isOpen;
  }

  get dayClosed(): boolean {
    return this.day && this.day.closed;
  }

  get hasUnsaved(): boolean {
    return this.sales.filter((sale: Full) => !sale.isSaved).length > 0;
  }

}
