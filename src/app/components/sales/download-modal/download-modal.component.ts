import {AfterViewInit, Component, Input} from "@angular/core";
import {SaleType} from "../../../models/sale/Sale";
import {getEnumKeys} from "../../../helpers/enum.helper";
import {SalesFormatType} from "../../../services/sales.service";
import {environment} from "../../../../environments/environment";
import {onlyDateToPHP} from "../../../helpers/date-formatter.helper";

@Component({
  selector: 'app-download-modal',
  templateUrl: './download-modal.component.html',
  styleUrls: ['./download-modal.component.css']
})
export class DownloadModalComponent implements AfterViewInit {
  @Input() startDate: Date;
  @Input() endDate: Date;
  @Input() pointId: number;
  @Input() type: SaleType;

  types: SaleType[];
  formatTypes: SalesFormatType[];

  format: SalesFormatType = SalesFormatType.CSV;

  constructor() {
    //TODO: PDF
    this.formatTypes = getEnumKeys(SalesFormatType, [], [SalesFormatType.PDF, SalesFormatType.HTML]);
    this.types = getEnumKeys(SaleType, [], [SaleType.ZREPORT]);
  }

  ngAfterViewInit(): void {
    $('[tooltip]').tooltip();
  }

  get link(): string {
    return downloadUrl(this.pointId, this.format, this.type, this.startDate, this.endDate);
  }

  get printLink(): string {
    return downloadUrl(this.pointId, SalesFormatType.HTML, this.type, this.startDate, this.endDate);
  }
}

function downloadUrl(pointId: number, format: SalesFormatType, type: SaleType, start: Date, end: Date): string {
  let startDate = onlyDateToPHP(start);
  let endDate = onlyDateToPHP(end);
  let saleType = SaleType[type].toLowerCase();
  let formatType = SalesFormatType[format].toLowerCase();
  let fileName = `${saleType}-${startDate}-${endDate}.${formatType}`;
  let token: string = localStorage.getItem('token');
  return `${environment.apiUrl}/download/${pointId}/${fileName}?token=${token}`;
}
