import {Component, EventEmitter, Input, Output} from "@angular/core";
import {User} from "../../../models/user";
import {SaleType} from "../../../models/sale/Sale";
import {DayStatus} from "../../../models/sale/digest/DayStatus";
import {Day} from "../../../models/sale/digest/Day";

@Component({
  selector: 'app-sale-header',
  templateUrl: './sale-header.component.html',
  styleUrls: ['./sale-header.component.scss']
})
export class SaleHeaderComponent {
  private _date: Date;

  @Input() day: Day;
  @Input() dayStatuses: DayStatus[] = [];
  @Input() user: User;
  @Input() importType: SaleType;
  @Input() templateEXCELUrl: string;
  @Input() checksSum: number;
  @Input() pointCurrency: string;
  @Input() sum: number;
  @Input() checksSumOfReturns: number;
  @Input() sumOfReturns: number;
  @Input() startDate: Date;
  @Input() endDate: Date;
  @Input() pointId: number;
  @Input() write: boolean;
  @Input() isHandleEnabled: boolean;
  @Input() isUploadEnabled: boolean;
  @Input() isBlocked: boolean;
  @Input() hasUnsaved: boolean;

  @Output('date') dateEmitter: EventEmitter<Date> = new EventEmitter<Date>();
  @Output() onUploadSuccess: EventEmitter<any> = new EventEmitter<any>();
  @Output() save: EventEmitter<any> = new EventEmitter<any>();
  @Output() upload: EventEmitter<any> = new EventEmitter<any>();
  @Output() unlock: EventEmitter<any> = new EventEmitter<any>();

  @Input() set date(date: Date) {
    if (date != null && date != this._date) {
      this._date = date;
      this.dateEmitter.emit(date);
    }
  }

  get date(): Date {
    return this._date;
  }

}
