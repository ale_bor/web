import {Component, Input} from "@angular/core";
import {PointDigest} from "../../../../models/sale/digest/PointDigest";
import {datesDiff} from "../../../../helpers/date.helper";
import {UserPoint} from "../../../../models/point";
import {NavbarService} from "../../../../services/navbar.service";

@Component({
  selector: '[app-sale-list-item]',
  templateUrl: './sale-list-item.component.html',
  styleUrls: ['./sale-list-item.component.scss']
})
export class SaleListItemComponent {

  @Input() pointDigest: PointDigest;
  @Input() currentDate: Date;

  constructor(private navbarService: NavbarService) {
  }

  //noinspection JSMethodCanBeStatic
  compareDates(date1: Date, date2: Date): number {
    return datesDiff(date1, date2);
  }

  changePoint(point: UserPoint) {
    this.navbarService.setSelectedPoint(point);
  }

}
