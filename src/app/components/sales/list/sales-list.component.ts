import {Component, OnDestroy, OnInit} from "@angular/core";
import {Location} from "@angular/common";
import {SalesService} from "../../../services/sales.service";
import {
  compareWithoutTime,
  dateMinus,
  datePlus,
  dateWithoutTime,
  getMonday,
  timestamp
} from "../../../helpers/date.helper";
import {PointDigest} from "../../../models/sale/digest/PointDigest";
import {NavbarService} from "../../../services/navbar.service";
import {AppService} from "../../../services/app.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-sales-list',
  templateUrl: './sales-list.component.html',
  styleUrls: ['./sales-list.component.scss'],
  providers: [SalesService]
})
export class SalesListComponent implements OnInit, OnDestroy {
  private _date: Date;
  digests: PointDigest[] = [];
  private dateSubscription: Subscription;

  constructor(private salesService: SalesService,
              private navbarService: NavbarService,
              private appService: AppService,
              private _location: Location) {
  }

  ngOnInit(): void {
    this.dateSubscription = this.navbarService.getDate().subscribe((date: Date) => {
      if (!compareWithoutTime(date, this._date)) {
        this._date = date;
        this.load();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.dateSubscription) {
      this.dateSubscription.unsubscribe();
    }
  }

  updateDate(days: number) {
    this.date = datePlus(days, this.date);
  }

  private load() {
    this.salesService.digest(this.appService, this.startDate, this.endDate)
      .subscribe((digests: PointDigest[]) => this.digests = digests);
  }

  get date(): Date {
    return this._date;
  }

  set date(value: Date) {
    this.navbarService.setDate(value);
  }

  //noinspection JSMethodCanBeStatic
  get currentDate(): Date {
    return dateWithoutTime();
  }

  get startDate(): Date {
    return dateMinus(7, this.monday);
  }

  private get monday() {
    return getMonday(dateWithoutTime(this._date));
  }

  get endDate(): Date {
    return datePlus(6, this.monday);
  }

  get days(): Date[] {
    let dates: Date[] = [];
    let date: Date = this.startDate;
    do {
      dates.push(date);
      date = datePlus(1, date);
    } while (!compareWithoutTime(date, datePlus(1, this.endDate)));

    return dates;
  }

  //noinspection JSMethodCanBeStatic
  compareDates(date1: Date, date2: Date): number {
    return timestamp(dateWithoutTime(date1)) - timestamp(dateWithoutTime(date2));
  }

  get hasNext(): boolean {
    return this.compareDates(dateWithoutTime(), dateWithoutTime(datePlus(6, this._date))) > 0;
  }

  goBack() {
    this._location.back();
  }
}
