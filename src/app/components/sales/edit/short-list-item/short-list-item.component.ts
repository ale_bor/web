import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Full} from "../../../../models/sale/Full";
import {OperationType} from "../../../../models/sale/Sale";
import {getEnumKeys} from "../../../../helpers/enum.helper";
import {PointCurrency} from "../../../../models/point";

@Component({
  selector: '[app-short-list-item]',
  templateUrl: './short-list-item.component.html',
  styleUrls: ['../list-item.scss']
})
export class ShortListItemComponent {

  @Input() sale: Full;
  @Input() id: number;
  @Input() dayClosed: boolean;
  @Input() write: boolean;
  @Input() currency: PointCurrency;
  @Output() deleteSale: EventEmitter<any> = new EventEmitter(true);
  operationTypes: number[];

  constructor() {
    this.operationTypes = getEnumKeys(OperationType, [OperationType.SALE, OperationType.RETURN]);
  }

}
