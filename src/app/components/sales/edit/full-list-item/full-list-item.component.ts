import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Full} from "../../../../models/sale/Full";
import {PointCurrency} from "../../../../models/point";
import {OperationType} from "../../../../models/sale/Sale";
import {getEnumKeys} from "../../../../helpers/enum.helper";

@Component({
  selector: '[app-full-list-item]',
  templateUrl: './full-list-item.component.html',
  styleUrls: ['../list-item.scss']
})
export class FullListItemComponent {

  @Input() sale: Full;
  @Input() id: number;
  @Input() dayClosed: boolean;
  @Input() write: boolean;
  @Input() currency: PointCurrency;
  @Output() deleteSale: EventEmitter<any> = new EventEmitter(true);
  @Output() toggleVisibility: EventEmitter<any> = new EventEmitter(true);
  @Output() removeItem: EventEmitter<any> = new EventEmitter(true);
  operationTypes: number[];

  constructor() {
    this.operationTypes = getEnumKeys(OperationType, [OperationType.SALE, OperationType.RETURN]);
  }

}
