import {Component} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {Full, FullItem} from "../../../models/sale/Full";
import {OperationType} from "../../../models/sale/Sale";
import {BasicSaleComponent} from "../basic-check.component";
import {NavbarService} from "../../../services/navbar.service";
import {SalesService} from "../../../services/sales.service";
import {AppService} from "../../../services/app.service";
import {setCurrentTime} from "../../../helpers/date.helper";
import {routeValueObservable} from "../../../helpers/route.helper";
import {PointCheckType} from "../../../models/point";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent extends BasicSaleComponent {

  get templateEXCELUrl(): string {
    if (this.checkType) {
      return `/assets/template-${PointCheckType[this.checkType].toLowerCase()}.xlsx`;
    } else {
      return `/assets/template-full.xlsx`;
    }
  }

  set templateEXCELUrl(val: string) {
  }

  public checkType: PointCheckType;

  constructor(navbarService: NavbarService,
              salesService: SalesService,
              route: ActivatedRoute,
              router: Router,
              appService: AppService) {
    super(navbarService, salesService, route, router, appService);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    routeValueObservable(this.route, 'checkType').subscribe((val: string) => this.checkType = PointCheckType[val.toUpperCase()]);
  }

  protected onSaveSuccess(originalSale: Full, savedSale: Full) {
    originalSale.items = savedSale.items;
  }

  public get sum(): number {
    let sum: number = 0;
    Observable.from(this.sales)
      .filter((report: Full) => Boolean(report.sum) && report.operationType === OperationType.SALE)
      .subscribe((report: Full) => sum += report.sum);
    return sum;
  }

  public get sumOfReturns(): number {
    let sum: number = 0;
    this.sales
      .filter((report: Full) => Boolean(report.sum) && report.operationType === OperationType.RETURN)
      .forEach((report: Full) => sum += report.sum);
    return sum;
  }

  public get checksSum(): number {
    let sum: number = 0;
    this.sales
      .filter((report: Full) => Boolean(report.sum) && report.operationType === OperationType.SALE)
      .forEach((report: Full) => sum++);
    return sum;
  }

  public get checksSumOfReturns(): number {
    let sum: number = 0;
    this.sales
      .filter((report: Full) => Boolean(report.sum) && report.operationType === OperationType.RETURN)
      .forEach((report: Full) => sum++);
    return sum;
  }

  protected get newSale(): Full {
    let till = this.sales.length > 0 ? this.sortedSales[0].till : "1";
    let saleItem = null;
    if (this.checkType == PointCheckType.SHORT) {
      saleItem = new FullItem(0, null, "-", "-", 0, 0, null);
    } else {
      saleItem = new FullItem(0, null, "", "", null, null, null);
    }
    return new Full(null,
      setCurrentTime(this._date),
      OperationType.SALE,
      this.user.id,
      this.point.id,
      String(this.nextSaleNumber()),
      till,
      [saleItem]
    );
  }

  removeItem(sale: Full, item: FullItem) {
    if (sale.items.length > 1) {
      sale.removeItem(item);
    } else {
      this.sales.splice(this.sales.indexOf(sale), 1);
    }
  }

  get isFull() {
    return this.checkType == PointCheckType.FULL;
  }

}
