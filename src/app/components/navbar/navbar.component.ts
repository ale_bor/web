import {Component, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {UserService} from "../../services/user.service";
import {User} from "../../models/user";
import {UsersService} from "../../services/users.service";
import {Action, ActionType, NavbarService} from "../../services/navbar.service";
import {UserPoint} from "../../models/point";
import {AppService} from "../../services/app.service";
import {compareWithoutTime} from "../../helpers/date.helper";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  inLoading: boolean = true;
  user: User;
  point: UserPoint;
  date: Date;
  private navbarActionSubscription: Subscription;
  private loadingSubscription: Subscription;
  private dateSubscription: Subscription;

  constructor(private navbarService: NavbarService,
              private userService: UserService,
              private usersService: UsersService,
              private appService: AppService) {
  }

  ngOnInit(): void {
    this.navbarActionSubscription = this.navbarService.actions.subscribe((a: Action) => {
      switch (a.type) {
        case ActionType.change:
          if (a.user) {
            this.user = a.user;
          }
          if (a.point) {
            this.point = a.point;
          }
          break;
        case ActionType.reload:
          this.load();
          break;
      }
    });
    this.loadingSubscription = this.appService.loading
      .subscribe((inLoading: boolean) => this.inLoading = inLoading);
    this.dateSubscription = this.navbarService.getDate().subscribe((date: Date) => {
      if (!compareWithoutTime(date, this.date)) {
        this.date = date;
      }
    });
    this.load();
  }

  ngOnDestroy(): void {
    if (this.navbarActionSubscription) {
      this.navbarActionSubscription.unsubscribe();
    }
    if (this.loadingSubscription) {
      this.loadingSubscription.unsubscribe();
    }
    if (this.dateSubscription) {
      this.dateSubscription.unsubscribe();
    }
  }

  load() {
    this.usersService.currentUser(this.appService).subscribe((user: User) => {
      this.navbarService.setUser(user);

      let lastPointId: number = Number(localStorage.getItem("lastPointId"));
      let filtered: UserPoint[] = user.points.filter((s: UserPoint) => s.id === lastPointId);
      let point: UserPoint = null;
      if (filtered.length > 0) {
        point = filtered[0];
      } else if (user.points.length > 0) {
        point = user.points[0];
        localStorage.setItem("lastPointId", String(point.point));
      }
      this.navbarService.setSelectedPoint(point);

      this.appService.stopLoading();
    }, () => this.appService.stopLoading(), () => this.appService.stopLoading());
  }

  deAuth(): void {
    this.userService.deAuth(this.appService).subscribe(() => {
      this.user = null;
      this.navbarService.action = new Action(ActionType.deAuth);
    });
  }

  changePoint(point: UserPoint) {
    this.navbarService.setSelectedPoint(point);
    localStorage.setItem("lastPointId", String(point.point));
  }

  get timestamp(): number {
    return this.date.getTime();
  }
}
