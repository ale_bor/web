import {Component, OnDestroy, OnInit} from "@angular/core";
import {AppService} from "../../services/app.service";
import {Action, ActionType, NavbarService} from "../../services/navbar.service";
import {Subscription} from "rxjs";
import {User} from "../../models/user";
import {UserPoint} from "../../models/point";
import {LogService} from "../../services/log.service";
import {Log, LogAction} from "../../models/Log";
import {dateMinus, dateWithoutTime} from "../../helpers/date.helper";
import {getEnumKeys} from "../../helpers/enum.helper";

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit, OnDestroy {
  private actionsSubscription: Subscription;
  private loadingSubscription: Subscription;
  private inLoading: boolean;
  private point: UserPoint;
  private user: User;
  private _logs: Log[];

  private _startDate: Date = dateMinus(7, dateWithoutTime());
  private _endDate: Date = dateWithoutTime();

  actions: number[] = [];
  action: number = -1;
  userName: string;
  pointName: string;

  constructor(private appService: AppService,
              private navbarService: NavbarService,
              private logService: LogService) {
    this.actions = getEnumKeys(LogAction);
  }

  ngOnInit() {
    this.actionsSubscription = this.navbarService.actions.subscribe((action: Action) => {
      switch (action.type) {
        case ActionType.reload:
        case ActionType.change:
          if (action.user) {
            this.user = action.user;
          }
          if (action.point) {
            this.point = action.point;
          }
          if (this.user && this.point) {
            this.load();
          }
      }
    });
    this.loadingSubscription = this.appService.loading
      .subscribe((inLoading: boolean) => this.inLoading = inLoading);
  }

  ngOnDestroy(): void {
    if (this.actionsSubscription) {
      this.actionsSubscription.unsubscribe();
    }
    if (this.loadingSubscription) {
      this.loadingSubscription.unsubscribe();
    }
  }

  set startDate(date: Date) {
    this._startDate = date;
    this.load();
  }

  get startDate(): Date {
    return this._startDate;
  }

  set endDate(date: Date) {
    this._endDate = date;
    this.load();
  }

  get endDate(): Date {
    return this._endDate;
  }

  private load() {
    this.logService.load(this.appService, this.startDate, this.endDate)
      .subscribe((logs: Log[]) => this._logs = logs);
  }

  get logs(): Log[] {
    if (!this._logs) {
      return null;
    }

    let result = this._logs;

    if (Number(this.action) !== -1) {
      result = result.filter((log: Log) => log.action == this.action);
    }

    if (this.userName && this.userName.length > 0) {
      result = result.filter((log: Log) => log.user).filter((log: Log) => {
        let isName = log.user.name.toLowerCase().includes(this.userName.toLowerCase());
        if (isName) {
          return true;
        }
        return log.user.id == Number(this.userName);
      });
    }

    if (this.pointName && this.pointName.length > 0) {
      result = result.filter((log: Log) => log.point).filter((log: Log) => {
        let isName = log.point.name.toLowerCase().includes(this.pointName.toLowerCase());
        if (isName) {
          return true;
        }
        let isRoomNumber = log.point.roomNumber.toLowerCase().includes(this.pointName.toLowerCase());
        if (isRoomNumber) {
          return true;
        }
        let isIdentifier = log.point.identifier.toLowerCase().includes(this.pointName.toLowerCase());
        if (isIdentifier) {
          return true;
        }
        return log.point.id == Number(this.pointName);
      });
    }

    return result;
  }
}
