import {Component, OnInit} from "@angular/core";
import {User} from "../../../models/user";
import {UsersService} from "../../../services/users.service";
import {AppService} from "../../../services/app.service";

@Component({
  selector: 'app-users-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class UsersListComponent implements OnInit {
  users: User[];

  constructor(private userListService: UsersService,
              private appService: AppService) {
  }

  ngOnInit(): void {
    this.userListService.users(this.appService)
      .subscribe((users: User[]) => this.users = users);
  }

}
