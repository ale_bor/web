import {Component, EventEmitter, Input, Output} from "@angular/core";
import {PointCheckType, UserInputType, UserPoint} from "../../../../models/point";
import {getEnumKeys} from "../../../../helpers/enum.helper";

@Component({
  selector: '[app-user-point]',
  templateUrl: './user-point.component.html',
  styleUrls: ['./user-point.component.scss']
})
export class UserPointComponent {
  @Input() point: UserPoint;
  @Input() isAdmin: boolean;
  @Output() deletePoint: EventEmitter<UserPoint> = new EventEmitter<UserPoint>();

  get inputTypes(): number[] {
    return getEnumKeys(UserInputType, this.point.checkType == PointCheckType.ZREPORT ? [UserInputType.HAND] : []);
  }

  isZReport(type: PointCheckType) {
    return type == PointCheckType.ZREPORT;
  }
}
