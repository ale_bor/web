import {Component, OnDestroy, OnInit} from "@angular/core";
import {Location} from "@angular/common";
import {User, UserRole, UserStatus} from "../../../models/user";
import {ActivatedRoute, Router} from "@angular/router";
import {UsersService} from "../../../services/users.service";
import {Point, UserPoint} from "../../../models/point";
import {PointsService} from "../../../services/points.service";
import {getEnumKeys} from "../../../helpers/enum.helper";
import {routeNumberValue} from "../../../helpers/route.helper";
import {Action, ActionType, NavbarService} from "../../../services/navbar.service";
import {Subscription} from "rxjs";
import {AlertAction, AlertType, AppService} from "../../../services/app.service";

@Component({
  selector: 'app-user-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class UserEditComponent implements OnInit, OnDestroy {
  userId: number;
  //Изменяемый
  user: User;
  //Текущий вошедший
  currentUser: User;
  roles: number[] = [];
  statuses: number[] = [];

  points: Point[];

  private subscription: Subscription;
  private loadingSubscription: Subscription;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private usersService: UsersService,
              private pointsService: PointsService,
              private navbarService: NavbarService,
              private appService: AppService,
              private _location: Location) {
    this.roles = getEnumKeys(UserRole);
    this.statuses = getEnumKeys(UserStatus);
    this.userId = routeNumberValue(route, 'id');
  }

  ngOnInit() {
    this.navbarService.getUser().subscribe((user: User) => {
      this.currentUser = user;

      if (user.id !== this.userId && !user.isAdmin) {
        this.router.navigate(['/users/edit', user.id]);
        return;
      }

      this.load();
    });
  }

  private load() {
    if (this.currentUser.isAdmin) {
      this.pointsService.points(this.appService)
        .subscribe((points: Point[]) => this.points = points,
          error => this.appService.alert(new AlertAction("Ошибка", error, AlertType.ERROR)));
      if (this.userId) {
        this.usersService.user(this.appService, this.userId)
          .subscribe((user: User) => this.user = user,
            error => this.appService.alert(new AlertAction("Ошибка", error, AlertType.ERROR)));
      } else {
        this.usersService.currentUser(this.appService).subscribe((currentUser: User) => {
            this.user = new User(0, "", "", [], UserRole.USER, UserStatus.NEW, currentUser.group);
            this.generatePassword();
          },
          error => this.appService.alert(new AlertAction("Ошибка", error, AlertType.ERROR)));
      }
    } else {
      this.user = this.currentUser;
    }
  }

  onSubmit() {
    if (this.userId) {
      this.usersService.update(this.appService, this.user)
        .subscribe((user: User) => {
            this.user = user;
            if (this.currentUser.id === user.id) {
              this.navbarService.action = new Action(ActionType.reload, user);
            }

            this.appService.alert(new AlertAction("Успешно", "Данные пользователя успешно сохранены", AlertType.DEFAULT));
          },
          error => this.appService.alert(new AlertAction("Ошибка", error, AlertType.ERROR)));
    } else {
      this.usersService.create(this.appService, this.user)
        .subscribe((user: User) => {
            this.router.navigate(['/users/edit/', user.id]);
            this.appService.alert(new AlertAction("Успешно", "Данные пользователя успешно сохранены", AlertType.DEFAULT));
          },
          error => this.appService.alert(new AlertAction("Ошибка", error, AlertType.ERROR)));
    }
  }

  deleteUser() {
    this.usersService.deleteUser(this.appService, this.user.id).subscribe(() => {
        this.router.navigate(['/users']);
        this.appService.alert(new AlertAction("Успешно", "Пользователь удалён", AlertType.DEFAULT));
      },
      error => this.appService.alert(new AlertAction("Ошибка", error, AlertType.ERROR)));
  }

  //noinspection JSUnusedGlobalSymbols
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.loadingSubscription) {
      this.loadingSubscription.unsubscribe();
    }
  }

  generatePassword() {
    this.user.password = Math.random().toString(36).slice(-6);
  }

  get searchPoint(): Point[] {
    return this.points.filter((point: Point) => this.user.points.map((point: UserPoint) => point.point).indexOf(point.id) == -1 && !point.isBlocked);
  }

  addPoint(point: Point) {
    if (this.user.points.map((point: UserPoint) => point.point).indexOf(point.id) > -1) {
      return;
    }
    this.user.points.push(point.simple);
  }

  deletePoint(item: UserPoint) {
    this.user.points = this.user.points.filter((i: UserPoint) => i !== item);
  }

  isNewUserStatus(status: UserStatus) {
    return UserStatus.NEW == status;
  }

  goBack() {
    this._location.back();
  }

}
