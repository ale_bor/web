import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {LoginFormModel, UserService} from "../../services/user.service";
import {DefaultResult} from "../../models/DefaultResult";
import {UserDataModel} from "../../models/user";
import {Action, ActionType, NavbarService} from "../../services/navbar.service";
import {AppService} from "../../services/app.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  model: LoginFormModel = {email: '', password: ''};
  error: string;

  constructor(private router: Router,
              private userService: UserService,
              private navbarService: NavbarService,
              private appService: AppService) {
  }

  onSubmit() {
    this.error = null;
    this.userService.auth(this.model, this.appService)
      .subscribe((res: DefaultResult<UserDataModel>) => {
        if (res.success) {
          this.navbarService.action = new Action(ActionType.reload);
          this.router.navigate(['/']);
        }
      }, error => this.error = error);
  }
}
