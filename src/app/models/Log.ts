import {User, UserDataModel} from "./user";
import {Point, PointDataModel} from "./point";
import {dateFromPHP} from "../helpers/date-formatter.helper";

export class Log {
  id: number;
  date: Date;
  action: LogAction;
  meta: any;
  user: User;
  point: Point;

  constructor(id: number, date: Date, action: LogAction, meta: any, user: User, point: Point) {
    this.id = id;
    this.date = date;
    this.action = action;
    this.meta = meta;
    this.user = user;
    this.point = point;
  }

  static fromModel(model: LogDataModel): Log {
    return new Log(
      Number(model.id),
      dateFromPHP(model.date),
      LogAction[model.action],
      model.meta,
      model.user !== null ? User.fromModel(model.user) : null,
      model.point !== null ? Point.fromModel(model.point) : null
    );
  }
}

export class LogDataModel {
  id: string;
  date: string;
  action: string;
  meta: any;
  user: UserDataModel;
  point: PointDataModel;
}

export enum LogAction {
  CREATE_SALE,
  REMOVE_SALE,
  CHANGE_SALE,

  CREATE_USER,
  CHANGE_USER,
  REMOVE_USER,

  CREATE_POINT,
  CHANGE_POINT,
  REMOVE_POINT,

  PARSE_EXCEL,
  CREATE_EXCEL,

  AUTH,
  DE_AUTH,
}
