import {Sale, SaleModel, SaleType} from "./Sale";
import {DayStatus} from "./digest/DayStatus";
import {dateFromPHP, dateToPHP} from "../../helpers/date-formatter.helper";

export class ZReport extends Sale {
  count: number;
  sum: number;
  status: DayStatus;

  constructor(id: number, date: Date, status: DayStatus, userId: number, pointId: number, count: number, sum: number) {
    super(id, date, userId, pointId, SaleType.ZREPORT);
    this.count = count;
    this.sum = sum;
    this.status = status;
  }

  static fromModel(model: ZReportModel): ZReport {
    return new ZReport(
      Number(model.id),
      dateFromPHP(model.date),
      DayStatus[model.status],
      Number(model.userId),
      Number(model.pointId),
      parseFloat(model.count),
      parseFloat(model.sum)
    );
  }

  get model(): ZReportModel {
    return {
      id: String(this.id),
      date: dateToPHP(this.date),
      status: DayStatus[this.status],
      userId: String(this.userId),
      pointId: String(this.pointId),
      type: SaleType[this.type],
      count: String(this.count),
      sum: String(this.sum)
    };
  }

  get isValid(): boolean {
    return this.status == DayStatus.CLOSED || this.status == DayStatus.BLOCKED || this.status == DayStatus.NOSALE;
  }
}

export interface ZReportModel extends SaleModel {
  count: string;
  sum: string;
  status: string;
}
