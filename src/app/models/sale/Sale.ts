import {compareWithoutSeconds} from "../../helpers/date.helper";
import {dateToPHP} from "../../helpers/date-formatter.helper";

export abstract class Sale {
  id: number;
  date: Date;
  userId: number;
  pointId: number;
  type: SaleType;

  isSaved: boolean = false;
  order: number = 0;

  constructor(id: number, date: Date, userId: number, pointId: number, type: SaleType) {
    this.id = id;
    this.date = date;
    this.userId = userId;
    this.pointId = pointId;
    this.type = type;
  }

  //noinspection JSUnusedLocalSymbols
  static fromModel(model: SaleModel): Sale {
    return null;
  }

  get model(): SaleModel {
    return {
      id: String(this.id),
      date: dateToPHP(this.date),
      userId: String(this.userId),
      pointId: String(this.pointId),
      type: SaleType[this.type],
    };
  }

  compare(s: Sale): boolean {
    return compareWithoutSeconds(s.date, this.date) && s.type === this.type;
  }

  abstract get isValid(): boolean;
}

export interface SaleModel {
  id: string;
  date: string;
  userId: string;
  pointId: string;
  type: string;
}

export enum SaleType {
  ZREPORT, SHORT, FULL
}

export enum OperationType {
  SALE, RETURN
}
