import {OperationType, Sale, SaleModel, SaleType} from "./Sale";
import {dateFromPHP, dateToPHP} from "../../helpers/date-formatter.helper";

export class Full extends Sale {
  number: string; //ноmер чека
  till: string; //касса
  items: FullItem[];
  isOpen: boolean = false;
  operationType: OperationType;

  constructor(id: number, date: Date, operationType: OperationType, userId: number, pointId: number, number: string, till: string, items: FullItem[]) {
    super(id, date, userId, pointId, SaleType.FULL);
    this.number = number;
    this.till = till;
    this.items = items;
    this.operationType = operationType;
  }

  static fromModel(model: FullModel): Full {
    return new Full(
      Number(model.id),
      dateFromPHP(model.date),
      OperationType[model.operationType],
      Number(model.userId),
      Number(model.pointId),
      model.number,
      model.till,
      model.items ? model.items.map((model: FullItemModel) => FullItem.fromModel(model)) : []
    );
  }

  get model(): FullModel {
    return {
      id: String(this.id),
      date: dateToPHP(this.date),
      operationType: OperationType[this.operationType],
      userId: String(this.userId),
      pointId: String(this.pointId),
      type: SaleType[this.type],
      number: String(this.number),
      till: String(this.till),
      items: this.items ? this.items.map((item: FullItem) => item.model) : [],
    };
  }

  get isValid(): boolean {
    return this.number && this.till && this.sum != null && this.sum > 0 && this.items && this.items.length > 0;
  }

  addItem(): void {
    this.items.push(new FullItem(0, this.id, "", "", 0, 0, 0));
  }

  removeItem(item: FullItem) {
    let index: number = this.items.indexOf(item);
    if (index !== -1) {
      this.items.splice(index, 1);
    }
  }

  get sum(): number {
    let sum: number = 0;
    this.items
      .map((i: FullItem) => Boolean(i.discount) && Number(i.discount) > 0 ? Number(i.discount) : Number(i.price) * Number(i.amount))
      .forEach((i: number) => sum += Number(i));
    return sum !== 0 ? sum : null;
  }

  set sum(sum: number) {
    if (this.items == null || this.items.length == 0) {
      this.items.push(new FullItem(null, this.id, "-", "-", sum, 1, 0));
    } else {
      this.items[0].price = sum;
      this.items[0].amount = 1;
    }
  }

  compare(s: Full): boolean {
    return super.compare(s)
      && s.sum === this.sum
      && s.operationType === this.operationType
      && s.till === this.till
      && s.number === this.number;
  }

  get isReturn(): boolean {
    return this.operationType == OperationType.RETURN;
  }
}

export interface FullModel extends SaleModel {
  number: string;
  till: string;
  type: string;
  items: FullItemModel[];
  operationType: string;
}

export class FullItemModel {
  id: string;
  saleId: string;
  article: string;
  name: string;
  price: string;
  amount: string;
  discount: string;
}

export class FullItem {
  id: number;
  saleId: number;
  article: string;
  name: string;
  price: number;
  amount: number;
  private _discount: number;

  constructor(id: number, saleId: number, article: string, name: string, price: number, amount: number, discount: number) {
    this.id = id;
    this.saleId = saleId;
    this.article = article;
    this.name = name;
    this.price = price;
    this.amount = amount;
    this._discount = discount;
  }

  get discount(): number {
    if (this._discount) {
      return this._discount;
    }
    if (this.price && this.amount) {
      return this.price * this.amount;
    }
    return null;
  }

  set discount(value: number) {
    this._discount = value;
  }

  get model(): FullItemModel {
    return {
      id: String(this.id),
      saleId: String(this.saleId),
      article: String(this.article),
      name: String(this.name),
      price: String(this.price),
      amount: String(this.amount),
      discount: String(this._discount),
    };
  }

  static fromModel(model: FullItemModel) {
    return new FullItem(
      Number(model.id),
      Number(model.saleId),
      model.article,
      model.name,
      Number(model.price),
      Number(model.amount),
      Number(model.discount),
    )
  }
}
