import {timestamp} from "../../../helpers/date.helper";
import {DayStatus} from "./DayStatus";
import {dateFromPHP} from "../../../helpers/date-formatter.helper";

export class DayDigest {
  date: Date;
  count: number;
  status: DayStatus;
  sum: number;
  closed: boolean;

  constructor(date: Date, count: number, status: DayStatus, sum: number, closed: boolean) {
    this.date = date;
    this.count = count;
    this.status = status;
    this.sum = sum;
    this.closed = closed;
  }

  get timestamp(): number {
    return timestamp(this.date);
  }

  get isStatusNoData(): boolean {
    return this.status === DayStatus.NODATA;
  }

  get isStatusSale(): boolean {
    return this.status === DayStatus.SALE;
  }

  get isStatusNoSale(): boolean {
    return this.status === DayStatus.NOSALE;
  }

  get isStatusClosed(): boolean {
    return this.status === DayStatus.CLOSED || this.status === DayStatus.E_WORK;
  }

  get isStatusBlocked(): boolean {
    return this.status === DayStatus.BLOCKED;
  }

  static fromModel(model: DayDigestModel) {
    return new DayDigest(
      dateFromPHP(model.date),
      Number(model.count),
      DayStatus[model.status],
      Number(model.sum),
      model.closed
    );
  }
}

export interface DayDigestModel {
  date: string;
  count: string;
  status: string;
  sum: string;
  closed: boolean;
}
