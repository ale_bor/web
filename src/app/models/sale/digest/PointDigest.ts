import {DayDigest, DayDigestModel} from "./DayDigest";
import {UserPoint, UserPointModel} from "../../point";

export class PointDigest {
  point: UserPoint;
  digests: DayDigest[] = [];

  isOpen: boolean = false;

  constructor(point: UserPoint, digest: DayDigest[]) {
    this.point = point;
    this.digests = digest;
  }

  static fromModel(model: PointDigestModel): PointDigest {
    return new PointDigest(
      UserPoint.fromModel(model.point),
      model.digests.map((model: DayDigestModel) => DayDigest.fromModel(model))
    );
  }
}

export interface PointDigestModel {
  point: UserPointModel;
  digests: DayDigestModel[];
}
