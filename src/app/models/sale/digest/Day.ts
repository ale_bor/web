import {DayStatus} from "./DayStatus";
import {timestamp} from "../../../helpers/date.helper";
import {dateFromPHP, dateToPHP} from "../../../helpers/date-formatter.helper";

export class Day {
  id: number;
  date: Date;
  status: DayStatus;
  closed: boolean;
  pointId: number;

  constructor(id: number, date: Date, status: DayStatus, closed: boolean, pointId: number) {
    this.id = id;
    this.date = date;
    this.status = status;
    this.closed = closed;
    this.pointId = pointId;
  }

  get timestamp(): number {
    return timestamp(this.date);
  }

  get isStatusNoData(): boolean {
    return this.status === DayStatus.NODATA;
  }

  get isStatusSale(): boolean {
    return this.status === DayStatus.SALE;
  }

  get isStatusNoSale(): boolean {
    return this.status === DayStatus.NOSALE;
  }

  get isStatusClosed(): boolean {
    return this.status === DayStatus.CLOSED || this.status === DayStatus.E_WORK;
  }

  get isStatusBlocked(): boolean {
    return this.status === DayStatus.BLOCKED;
  }

  get model(): DayModel {
    return {
      id: String(this.id),
      date: dateToPHP(this.date),
      status: DayStatus[this.status],
      closed: this.closed ? "true" : "false",
      pointId: String(this.pointId),
    };
  }

  static fromModel(model: DayModel) {
    return new Day(
      Number(model.id),
      dateFromPHP(model.date),
      DayStatus[model.status],
      String(model.closed) == "true",
      Number(model.pointId),
    );
  }
}

export interface DayModel {
  id: string;
  date: string;
  status: string;
  closed: string;
  pointId: string;
}
