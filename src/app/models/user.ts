import {Group} from "./group";
import {UserPoint, UserPointModel} from "./point";

export class User {
  id: number;
  name: string;
  email: string;
  password?: string = null;
  points: UserPoint[];
  role: UserRole;
  status: UserStatus;
  group: Group;

  constructor(id: number, name: string, email: string, points: UserPoint[], role: UserRole, status: UserStatus, group: Group) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.points = points;
    this.role = role;
    this.status = status;
    this.group = group;
  }

  static fromModel(model: UserDataModel) {
    return new User(
      Number(model.id),
      model.name,
      model.email,
      model.points.map((model: UserPointModel) => UserPoint.fromModel(model)),
      UserRole[model.role],
      UserStatus[model.status],
      model.group
    );
  }

  get model(): UserDataModel {
    return {
      name: this.name,
      email: this.email,
      password: this.password,
      points: this.points.map((point: UserPoint) => point.model),
      role: UserRole[this.role],
      status: UserStatus[this.status],
      group: this.group,
    };
  }

  get isBlocked(): boolean {
    return this.status === UserStatus.BLOCKED;
  }

  get isAdmin(): boolean {
    const role = this.role;
    return role === UserRole.ADMIN || role === UserRole.SUPERADMIN || role === UserRole.GOD;
  }

  get isGod(): boolean {
    return this.role === UserRole.GOD;
  }

  get canBeDelete(): boolean {
    return this.status == UserStatus.NEW;
  }
}

export interface UserDataModel {
  id?: string | number
  name?: string;
  email?: string;
  password?: string;
  points?: UserPointModel[];
  role?: string;
  status?: string;
  group?: Group;
}

export enum UserRole {
  USER, ADMIN, SUPERADMIN, GOD
}

export enum UserStatus {
  NEW, DEFAULT, BLOCKED
}
