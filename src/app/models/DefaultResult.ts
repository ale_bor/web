import {dateFromPHP} from "../helpers/date-formatter.helper";

export abstract class DefaultResultModel<D> {
  readonly status: string;
  readonly meta: MetaModel;
  readonly data: D;
  readonly _d: any;
  readonly error: string;
}

export class DefaultResult<D> {
  readonly status: Status;
  readonly meta: Meta;
  readonly data: D;
  readonly _d: any;
  readonly error: string;

  constructor(status: Status, error: string, data: D, meta: Meta, _d: any) {
    this.status = status;
    this.error = error;
    this.data = data;
    this.meta = meta;
    this._d = _d;
  }

  get success(): boolean {
    return this.status == Status.ok;
  }

  get needAuth(): boolean {
    return this.status == Status.authRequired;
  }

  get noAccess(): boolean {
    return this.status == Status.NoAccess;
  }

  static fromModel<D>(model: DefaultResultModel<D>): DefaultResult<D> {
    return new DefaultResult(
      Status[model.status],
      model.error,
      model.data,
      Meta.fromModel(model.meta),
      model._d
    );
  }
}

export enum Status {
  /**
   * Операция успешно завершена
   */
  ok,
    /**
     * Требуется авторизация (пользователь не авторизован)
     */
  authRequired,
    /**
     * Нет прав доступа (client ломится в SA.например)
     */
  NoAccess,
    /**
     * Ошибка.
     */
  error
}

export abstract class MetaModel {
  readonly tokenString: string;
  readonly date: string;
  readonly notReceived: any[];
  readonly timezone: string;
}

export class Meta {
  readonly tokenString: string;
  readonly timestamp: Date;
  readonly notReceived: any[];
  readonly timezone: string;

  constructor(tokenString: string, date: Date, notReceived: any[], timezone: string) {
    this.tokenString = tokenString;
    this.timestamp = date;
    this.notReceived = notReceived;
    this.timezone = timezone;
  }

  static fromModel(model: MetaModel): Meta {
    return new Meta(
      model.tokenString,
      dateFromPHP(model.date),
      model.notReceived,
      model.timezone
    );
  }
}
