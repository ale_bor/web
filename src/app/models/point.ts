export class Point {
  id: number;
  name: string;
  owner: number;
  roomNumber: string;
  identifier: string;
  status: PointStatus;
  currency: PointCurrency;
  checkType: PointCheckType;

  constructor(id: number, name: string, owner: number, roomNumber: string, identifier: string, status: PointStatus, currency: PointCurrency, checkType: PointCheckType) {
    this.id = id;
    this.name = name;
    this.owner = owner;
    this.roomNumber = roomNumber;
    this.identifier = identifier;
    this.status = status;
    this.currency = currency;
    this.checkType = checkType;
  }

  static fromModel(model: PointDataModel): Point {
    return new Point(
      model.id,
      model.name,
      model.owner,
      model.roomNumber,
      model.identifier,
      PointStatus[model.status],
      PointCurrency[model.currency],
      PointCheckType[model.checkType],
    );
  }

  get model(): PointDataModel {
    return {
      id: this.id,
      name: this.name,
      owner: this.owner,
      roomNumber: this.roomNumber,
      identifier: this.identifier,
      status: PointStatus[this.status],
      currency: PointCurrency[this.currency],
      checkType: PointCheckType[this.checkType]
    };
  }

  get simple(): UserPoint {
    return new UserPoint(
      null,
      this.id,
      this.name,
      this.roomNumber,
      false,
      this.currency,
      this.checkType,
      UserInputType.HAND_AND_UPLOAD,
      this.status
    );
  }

  get canBeDelete(): boolean {
    return this.status == PointStatus.NEW;
  }

  get isBlocked(): boolean {
    return this.status == PointStatus.BLOCKED;
  }
}

export interface PointDataModel {
  id: number;
  name: string;
  owner: number;
  roomNumber: string;
  identifier: string;
  status: string;
  currency: string;
  checkType: string;
}

export class UserPoint {
  id?: number;
  point: number;
  name: string;
  roomNumber: string;
  write: boolean;
  currency: PointCurrency;
  checkType: PointCheckType;
  inputType: UserInputType;
  status: PointStatus;

  constructor(id: number, point: number, name: string, roomNumber: string, write: boolean, currency: PointCurrency, checkType: PointCheckType, inputType: UserInputType, status: PointStatus) {
    this.id = id;
    this.point = point;
    this.name = name;
    this.roomNumber = roomNumber;
    this.write = write;
    this.currency = currency;
    this.checkType = checkType;
    this.inputType = inputType;
    this.status = status;
  }

  static fromModel(model: UserPointModel) {
    return new UserPoint(
      Number(model.id),
      Number(model.point),
      model.name,
      model.roomNumber,
      model.write,
      PointCurrency[model.currency],
      PointCheckType[model.checkType],
      UserInputType[model.inputType],
      PointStatus[model.status],
    );
  }

  get model(): UserPointModel {
    return {
      id: this.id,
      point: this.point,
      name: this.name,
      roomNumber: this.roomNumber,
      write: this.write,
      currency: PointCurrency[this.currency],
      checkType: PointCheckType[this.checkType],
      inputType: UserInputType[this.inputType],
      status: PointStatus[this.status],
    };
  }

  get isHandleEnabled(): boolean {
    return [UserInputType.HAND, UserInputType.HAND_AND_UPLOAD].indexOf(this.inputType) > -1;
  }

  get isUploadEnabled(): boolean {
    return [UserInputType.UPLOAD, UserInputType.HAND_AND_UPLOAD].indexOf(this.inputType) > -1;
  }

  get isBlocked(): boolean {
    return this.status == PointStatus.BLOCKED;
  }
}

export interface UserPointModel {
  id?: string | number;
  point: string | number;
  name: string;
  roomNumber: string;
  write: boolean;
  currency: string;
  checkType: string;
  inputType: string;
  status: string;
}

export enum PointStatus {
  ACTIVE, BLOCKED, NEW, CLOSED
}

export enum PointCurrency {
  RUB, USD, EUR, KZT
}

export enum PointCheckType {
  FULL, SHORT, ZREPORT
}

export enum UserInputType {
  HAND, UPLOAD, HAND_AND_UPLOAD
}
