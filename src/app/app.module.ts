import {BrowserModule} from "@angular/platform-browser";
import {LOCALE_ID, NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {RouterModule} from "@angular/router";
import {Ng2UploaderModule} from "ng2-uploader/src/module/ng2-uploader.module";
import {AppComponent} from "./app.component";
import * as components from "./components/components";
import * as pipes from "./pipes/pipes";
import * as services from "./services/services";
import * as modules from "./modules/modules";
import {InputNumberDirective} from "./directives/input-number.directive";

@NgModule({
  imports: [
    FormsModule,
    RouterModule.forRoot([
      {path: 'login', component: components.LoginComponent},
      {
        path: 'users',
        children: [
          {path: '', component: components.UsersListComponent},
          {
            path: 'edit', children: [
            {path: '', component: components.UserEditComponent},
            {path: ':id', component: components.UserEditComponent},
          ]
          },
        ]
      },
      {
        path: 'groups',
        children: [
          {path: '', component: components.GroupsListComponent},
          {
            path: 'edit', children: [
            {path: '', component: components.GroupEditComponent},
            {path: ':id', component: components.GroupEditComponent},
          ]
          },
        ]
      },
      {
        path: 'points',
        children: [
          {path: '', component: components.PointsListComponent},
          {
            path: 'edit', children: [
            {path: '', component: components.PointEditComponent},
            {path: ':id', component: components.PointEditComponent},
          ]
          },
        ]
      },
      {
        path: 'sales',
        children: [
          {path: '', component: components.SalesListComponent},
          //TODO: Доделать
          {path: ':date', component: components.SalesListComponent},
          {
            path: 'edit',
            children: [
              {
                path: ':pointId',
                children: [
                  {path: 'zreport/:date', component: components.ZReportEditComponent},
                  {
                    path: ':checkType',
                    children: [
                      {path: ':date', component: components.EditComponent}
                    ]
                  },
                ]
              },
            ]
          },
        ]
      },
      {path: 'log', component: components.LogComponent},
      {path: '', component: components.HomeComponent},
      {path: '**', component: components.PageNotFoundComponent}
    ]),
    BrowserModule,
    HttpModule,
    modules.RedDatePickerModule,
    modules.RedTimePickerModule,
    Ng2UploaderModule
  ],
  declarations: [
    AppComponent,
    components.PointsListComponent,
    components.NavbarComponent,
    components.LoginComponent,
    components.HomeComponent,
    components.PageNotFoundComponent,
    components.UsersListComponent,
    components.UserEditComponent,
    components.PointEditComponent,
    components.ZReportEditComponent,
    components.SalesListComponent,
    components.GroupsListComponent,
    components.GroupEditComponent,
    components.DownloadModalComponent,
    components.SaleHeaderComponent,
    components.SalePageFooterComponent,
    components.UploadModalComponent,
    components.GodSettingsComponent,
    components.LogComponent,
    components.UserPointComponent,
    components.SaleListItemComponent,
    components.SaleColorLegendComponent,
    components.ZReportListItemComponent,
    components.ShortListItemComponent,
    components.FullListItemComponent,
    components.EditComponent,
    pipes.PointStatusPipe,
    pipes.UserRolePipe,
    pipes.CheckPipe,
    pipes.UserStatusPipe,
    pipes.SaleTypePipe,
    pipes.SaleOperationTypePipe,
    pipes.SalesFormatTypePipe,
    pipes.DayStatusPipe,
    pipes.PointCurrencyPipe,
    pipes.LogActionPipe,
    pipes.UserInputTypePipe,
    pipes.EditorUriPipe,
    InputNumberDirective
  ],
  providers: [
    services.UserService,
    services.UsersService,
    services.PointsService,
    services.GroupsService,
    services.NavbarService,
    services.SalesService,
    services.AppService,
    services.LogService,
    {provide: LOCALE_ID, useValue: "ru-RU"}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
