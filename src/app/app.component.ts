import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {Subscription} from "rxjs";
import {AlertAction, AlertType, AppService, ConfirmAction} from "./services/app.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('alertModal') alertModal: ElementRef;
  @ViewChild('loadingModal') loadingModal: ElementRef;
  @ViewChild('confirmModal') confirmModal: ElementRef;

  modalContent: string;
  modalTitle: string;

  confirmModalTitle: string;
  confirmModalContent: string;
  confirmModalAccept: string;

  private confirmAction: ConfirmAction;

  private alertActionSubscription: Subscription;
  private loadingSubscription: Subscription;
  private confirmSubscription: Subscription;

  private alertModalElement: JQuery;
  private loadingModalElement: JQuery;
  private confirmModalElement: JQuery;

  private inLoading: boolean = true;

  constructor(private appService: AppService) {
  }

  ngOnInit(): void {
    this.alertActionSubscription = this.appService.alertActions.subscribe((action: AlertAction) => {
      this.modalTitle = action.title;
      this.modalContent = action.content;
      //TODO: Implement
      switch (action.type) {
        case AlertType.DEFAULT:
          break;
      }

      this.alertModalElement.modal('show');
    });

    this.loadingSubscription = this.appService.loading.subscribe((inLoading: boolean) => {
      this.inLoading = inLoading;
      if (this.loadingModalElement) {
        this.loadingModalElement.modal(inLoading ? 'show' : 'hide');
      }
    });

    this.confirmSubscription = this.appService.confirmation.subscribe((action: ConfirmAction) => {
      this.confirmModalTitle = action.title;
      this.confirmModalContent = action.content;
      this.confirmModalAccept = action.accept;
      this.confirmAction = action;
      //TODO: Implement
      switch (action.type) {
        case AlertType.DEFAULT:
          break;
      }

      this.confirmModalElement.modal('show');
    });
  }

  ngAfterViewInit(): void {
    this.alertModalElement = $(this.alertModal.nativeElement);
    this.alertModalElement.modal({show: false});

    this.loadingModalElement = $(this.loadingModal.nativeElement);
    this.loadingModalElement.modal({show: this.inLoading, keyboard: false, backdrop: false});

    this.confirmModalElement = $(this.confirmModal.nativeElement);
    this.confirmModalElement.modal({show: false});
  }

  ngOnDestroy(): void {
    if (this.alertActionSubscription) {
      this.alertActionSubscription.unsubscribe();
    }
    if (this.loadingSubscription) {
      this.loadingSubscription.unsubscribe();
    }
  }

  cancelConfirmation() {
    this.confirmAction.result(false);
  }

  acceptConfirmation() {
    this.confirmAction.result(true);
  }
}

