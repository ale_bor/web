import {Pipe, PipeTransform} from "@angular/core";
import {UserStatus} from "../models/user";

@Pipe({
  name: 'userStatus'
})
export class UserStatusPipe implements PipeTransform {

  transform(value: UserStatus): string {
    switch (Number(value)) {
      case UserStatus.NEW:
        return "Новый пользователь";
      case UserStatus.DEFAULT:
        return "Разблокирован";
      case UserStatus.BLOCKED:
        return "Заблокирован";
      default:
        return "Неизвестный статус";
    }
  }

}
