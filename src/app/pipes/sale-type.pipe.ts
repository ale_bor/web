import {Pipe, PipeTransform} from "@angular/core";
import {SaleType} from "../models/sale/Sale";

@Pipe({
  name: 'saleType'
})
export class SaleTypePipe implements PipeTransform {

  transform(value: SaleType): string {
    switch (Number(value)) {
      case SaleType.ZREPORT:
        return "Z-Report отчёт за день";
      case SaleType.SHORT:
        return "Краткий отчёт за день";
      case SaleType.FULL:
        return "Полный отчёт за день";
      default:
        return "Неизвестный вид отчёта";
    }
  }

}
