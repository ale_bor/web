import {Pipe, PipeTransform} from "@angular/core";
import {PointStatus} from "../models/point";

@Pipe({
  name: 'pointStatus'
})
export class PointStatusPipe implements PipeTransform {

  transform(value: PointStatus, date: number): string {
    switch (Number(value)) {
      case PointStatus.ACTIVE:
        return "Активна";
      case PointStatus.BLOCKED:
        return `Заблокирована ${date ? date : ''}`;
      case PointStatus.NEW:
        return "Новая";
      case PointStatus.CLOSED:
        return "Закрыта";
      default:
        return "~";
    }
  }
}
