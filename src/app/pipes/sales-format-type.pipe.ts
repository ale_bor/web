import {Pipe, PipeTransform} from "@angular/core";
import {SalesFormatType} from "../services/sales.service";

@Pipe({
  name: 'salesFormatType'
})
export class SalesFormatTypePipe implements PipeTransform {

  transform(value: number): string {
    return SalesFormatType[Number(value)];
  }

}
