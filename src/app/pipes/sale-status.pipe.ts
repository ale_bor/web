import {Pipe, PipeTransform} from "@angular/core";
import {OperationType} from "../models/sale/Sale";

@Pipe({
  name: 'saleOperationType'
})
export class SaleOperationTypePipe implements PipeTransform {

  transform(value: OperationType): string {
    switch (Number(value)) {
      case OperationType.SALE:
        return "Продажа";
      case OperationType.RETURN:
        return "Возврат";
      default:
        return "-";
    }
  }
}
