import {Pipe, PipeTransform} from "@angular/core";
import {LogAction} from "../models/Log";

@Pipe({
  name: 'logAction'
})
export class LogActionPipe implements PipeTransform {

  transform(value: LogAction): string {
    switch (value) {
      case LogAction.CREATE_SALE:
        return "Создание чека";
      case LogAction.REMOVE_SALE:
        return "Удаление чека";
      case LogAction.CHANGE_SALE:
        return "Изменение чека";
      case LogAction.CREATE_USER:
        return "Создание пользователя";
      case LogAction.CHANGE_USER:
        return "Изменение пользователя";
      case LogAction.REMOVE_USER:
        return "Удаление пользователя";
      case LogAction.CREATE_POINT:
        return "Создание объекта";
      case LogAction.CHANGE_POINT:
        return "Изменение объекта";
      case LogAction.REMOVE_POINT:
        return "Удаление объекта";
      case LogAction.PARSE_EXCEL:
        return "Загрузка EXCEL файла";
      case LogAction.CREATE_EXCEL:
        return "Скачивание/печать EXCEL файла";
      case LogAction.AUTH:
        return "Авторизация";
      case LogAction.DE_AUTH:
        return "Выход";
      default:
        return LogAction[Number(value)];
    }
  }

}
