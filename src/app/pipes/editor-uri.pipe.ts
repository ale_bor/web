import {Pipe, PipeTransform} from "@angular/core";
import {Point, UserPoint, PointCheckType} from "../models/point";
import {timestamp, dateWithoutTime} from "../helpers/date.helper";

@Pipe({
  name: 'editorUri'
})
export class EditorUriPipe implements PipeTransform {

  transform(point: Point|UserPoint, currentTimestamp?: boolean): string {
    let pointId = point instanceof UserPoint ? point.point : point.id;
    return `/sales/edit/${pointId}/${PointCheckType[point.checkType].toLowerCase()}/` + (currentTimestamp ? timestamp(dateWithoutTime()) : '');
  }
}
