import {Pipe, PipeTransform} from "@angular/core";
import {DayStatus} from "../models/sale/digest/DayStatus";

@Pipe({
  name: 'dayStatus'
})
export class DayStatusPipe implements PipeTransform {
  transform(value: DayStatus): string {
    switch (Number(value)) {
      case DayStatus.NODATA:
        return "Нет данных";
      case DayStatus.SALE:
        return "Данные продаж введены";
      case DayStatus.NOSALE:
        return "Нет продаж";
      case DayStatus.CLOSED:
        return "Временно закрыт";
      case DayStatus.E_WORK:
        return "Тех. работы";
      case DayStatus.BLOCKED:
        return "Объект заблокирован";
      default:
        return "Неизвестный статус";
    }
  }
}
