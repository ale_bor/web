import {Pipe, PipeTransform} from "@angular/core";
import {UserInputType} from "../models/point";

@Pipe({
  name: 'userInputType'
})
export class UserInputTypePipe implements PipeTransform {

  transform(value: UserInputType): string {
    switch (Number(value)) {
      case UserInputType.HAND:
        return "Ручной";
      case UserInputType.UPLOAD:
        return "Файл";
      case UserInputType.HAND_AND_UPLOAD:
        return "Ручной и файл";
      default:
        return "Неизвестный метод ввода";
    }
  }

}
