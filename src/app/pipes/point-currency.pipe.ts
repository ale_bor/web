import {Pipe, PipeTransform} from "@angular/core";
import {PointCurrency} from "../models/point";

@Pipe({
  name: 'pointCurrency'
})
export class PointCurrencyPipe implements PipeTransform {
  transform(value: PointCurrency, args?: "name"): string {
    value = Number(value);
    if (!args) {
      switch (value) {
        case PointCurrency.USD:
          return "&#36;";
        case PointCurrency.EUR:
          return "&#8364;";
        case PointCurrency.RUB:
          return "&#8381;";
        case PointCurrency.KZT:
          return "&#8376;";
        default:
          return "?";
      }
    } else {
      return PointCurrency[value];
    }
  }
}
