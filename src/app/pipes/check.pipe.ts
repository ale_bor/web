import {Pipe, PipeTransform} from "@angular/core";
import {PointCheckType} from "../models/point";

@Pipe({
  name: 'checkType'
})
export class CheckPipe implements PipeTransform {

  transform(value: PointCheckType): string {
    switch (Number(value)) {
      case PointCheckType.FULL:
        return "Полный чек";
      case PointCheckType.SHORT:
        return "Краткий чек";
      case PointCheckType.ZREPORT:
        return "Z-Report чек";
      default:
        return "Неизвестный тип чека";
    }
  }

}
