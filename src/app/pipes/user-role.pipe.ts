import {Pipe, PipeTransform} from "@angular/core";
import {UserRole} from "../models/user";

@Pipe({
  name: 'userRole'
})
export class UserRolePipe implements PipeTransform {

  transform(value: UserRole): string {
    switch (Number(value)) {
      case UserRole.USER:
        return "Пользователь";
      case UserRole.ADMIN:
        return "Администратор";
      case UserRole.SUPERADMIN:
        return "Администратор групп";
      case UserRole.GOD:
        return "Глобальность";
      default:
        return "Никто";
    }
  }
}
