export function dateWithoutTime(date: Date = new Date()): Date {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

export function dateWithoutSeconds(date: Date = new Date()): Date {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes());
}

export function currentDate(): Date {
  return dateWithoutTime(new Date());
}

export function timestamp(date: Date = new Date()): number {
  return date.getTime();
}

export function datePlus(days: number, date: Date = new Date()): Date {
  let currentDate: Date = dateWithoutTime(date);
  currentDate.setDate(currentDate.getDate() + days);
  return currentDate;
}

export function dateMinus(days: number, date: Date = new Date()): Date {
  return datePlus(-days, date);
}

export function timestampPlus(days: number, date: Date = new Date()): number {
  return timestamp(datePlus(days, date));
}

export function getMonday(d: Date = new Date()): Date {
  let day: number = d.getDay();
  let diff: number = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}

export function compareWithoutTime(date: Date, date2: Date): boolean {
  if (!date || !date2) {
    return false;
  }
  return timestamp(dateWithoutTime(date)) == timestamp(dateWithoutTime(date2));
}

export function compareWithoutSeconds(date: Date, date2: Date): boolean {
  if (!date || !date2) {
    return false;
  }
  return timestamp(dateWithoutSeconds(date)) == timestamp(dateWithoutSeconds(date2));
}

export function datesDiff(date1: Date, date2: Date): number {
  return timestamp(dateWithoutTime(date1)) - timestamp(dateWithoutTime(date2));
}

export function setCurrentTime(date: Date): Date {
  let now = new Date();
  now.setFullYear(date.getFullYear(), date.getMonth(), date.getDate());
  return now;
}
