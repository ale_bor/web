import {Router} from "@angular/router";
import {BehaviorSubject, Observable} from "rxjs";
import {DefaultResult, DefaultResultModel} from "../models/DefaultResult";
import {HttpHelper} from "./http.helper";
import {environment} from "../../environments/environment";

export function handleDefaultResult<D>(res: { json(): DefaultResultModel<D> }, router?: Router): DefaultResult<D> {
  let result = <DefaultResult<D>> DefaultResult.fromModel<D>(res.json());
  return handleStatusError(result, router);
}

export function handleStatusError<D>(result: DefaultResult<D>, router?: Router): DefaultResult<D> {
  let errorString: string;

  if (!result.success) {
    if (result.needAuth) {
      errorString = "Необходима авторизация";
      if (router) {
        router.navigate(["/login"]);
      }
    } else if (result.noAccess) {
      errorString = "Недостаточно прав";
      if (router) {
        router.navigate(["/"]);
      }
    } else {
      errorString = result.error;
    }
  }
  if (errorString) {
    throw errorString;
  }

  return result;
}

export function handleData<D>(result: DefaultResult<D>): D {
  return result.data;
}

export function notNull(object): boolean {
  return object !== null;
}

export function RXNotNull<D>(r: Observable<D> | BehaviorSubject<D>): Observable<D> {
  if (r instanceof BehaviorSubject) {
    r = (<BehaviorSubject<D>>r).asObservable();
  }

  return r.filter(notNull);
}

export interface LoadingListener {
  startLoading(): void;

  stopLoading(force?: boolean): void;
}

export function process<T>(src: Observable<T> | HttpHelper<T>, loadingListener?: LoadingListener, f?: (t: T) => void): Observable<T> {
  if (loadingListener) {
    loadingListener.startLoading();
  }
  let result: BehaviorSubject<T> = new BehaviorSubject(null);
  let forProcess: Observable<T>;
  if (src instanceof HttpHelper) {
    forProcess = src.build();
  } else {
    forProcess = <Observable<T>>src;
  }
  forProcess.subscribe((t: T) => {
    if (!environment.production) {
      console.debug('next');
    }
    if (f) {
      f(t);
    }
    result.next(t);
  }, error => {
    if (!environment.production) {
      console.debug('error', error);
    }
    result.error(error);
    if (loadingListener) {
      loadingListener.stopLoading();
    }
  }, () => {
    if (!environment.production) {
      console.debug('complete');
    }
    result.complete();
    if (loadingListener) {
      loadingListener.stopLoading();
    }
  });
  return RXNotNull(result);
}
