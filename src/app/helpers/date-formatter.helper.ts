export function dateToPHP(date: Date): string {
  let year = date.getFullYear();
  let month = ('0' + (date.getMonth() + 1)).slice(-2);
  let day = ('0' + date.getDate()).slice(-2);
  let hours = ('0' + (date.getHours())).slice(-2);
  let minutes = ('0' + date.getMinutes()).slice(-2);
  let seconds = ('0' + date.getSeconds()).slice(-2);

  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

export function onlyDateToPHP(date: Date): string {
  let year = date.getFullYear();
  let month = ('0' + (date.getMonth() + 1)).slice(-2);
  let day = ('0' + date.getDate()).slice(-2);

  return `${year}-${month}-${day}`;
}

export function dateFromPHP(dateString: string) {
  if (dateString.includes(' ')) {
    const matches = dateString.split(/[\-.,\/ :]/);
    const day = matches[0];
    const month = matches[1];
    const year = matches[2];
    const hour = matches[3];
    const minute = matches[4];
    const second = matches[5];
    return new Date(
      Number(year),
      Number(month) - 1,
      Number(day),
      Number(hour),
      Number(minute),
      Number(second),
      0
    );
  } else {
    if (isTime(dateString)) {
      return onlyTimeFromPHP(dateString);
    } else if (isDate(dateString)) {
      return onlyDateFromPHP(dateString);
    } else {
      return null;
    }
  }
}

function isDate(src: string): boolean {
  if (!src) {
    return false;
  }
  let matches = src.match(/^((3[0-1])|([0-2]?\d))[.,\- \/]((0?[1-9])|(1[0-2]))[.,\- \/]\d{2}\d{0,2}$/);
  return matches && matches.length > 0;
}

function isTime(src: string): boolean {
  if (!src) {
    return false;
  }
  let matches = src.match(/^([0-1]?\d|2[0-3])?:[0-5]?\d(:[0-5]?\d){0,2}$/);
  return matches && matches.length > 0;
}

function onlyDateFromPHP(src: string): Date {
  let matches = src.split(/[.,\- \/]/);
  const day = matches[0];
  const month = matches[1];
  const year = matches[2];
  return new Date(Number(year), Number(month) - 1, Number(day), 0, 0, 0, 0);
}

function onlyTimeFromPHP(src: string): Date {
  let date = src.split(':');
  return new Date(1970, 0, 1, Number(date[0]), Number(date[1]), Number(date[1]), 0);
}
