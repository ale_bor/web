import {Observable} from "rxjs";

function contains(x: any, src: any[]) {
  return src.indexOf(x) > -1;
}

export function getEnumKeys(enumClass: Object, filter: number[] = null, exclude: number[] = null) {
  let result: number[] = [];
  var numbersOfEnum: Observable<any> = Observable.from(Object.keys(enumClass))
    .filter(x => parseInt(x, 10) >= 0)
    .map(x=>parseInt(x));

  if (filter && filter.length > 0) {
    numbersOfEnum = numbersOfEnum.filter(x=>contains(x, filter));
  }
  if (exclude && exclude.length > 0) {
    numbersOfEnum = numbersOfEnum.filter(x=>!contains(x, exclude));
  }

  numbersOfEnum
    .toArray()
    .subscribe((values: number[]) => result = values);
  return result;
}
