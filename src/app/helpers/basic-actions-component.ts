import {ActivatedRoute, Router} from "@angular/router";
import {OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {NavbarService} from "../services/navbar.service";
import {UserPoint} from "../models/point";
import {User} from "../models/user";
import {getEnumKeys} from "./enum.helper";
import {compareWithoutTime, dateWithoutTime, timestamp} from "./date.helper";
import {dateRouteValueObservable, routeValueObservable} from "./route.helper";
import {EditorUriPipe} from "../pipes/editor-uri.pipe";
import {DayStatus} from "../models/sale/digest/DayStatus";

export abstract class BasicActionsComponent implements OnInit, OnDestroy {

  public dayStatuses: DayStatus[];
  public user: User;
  public point: UserPoint;
  public pointId: number;

  protected _date: Date;

  protected actionsSubscription: Subscription;
  protected dateSubscription: Subscription;
  protected pointIdSubscription: Subscription;

  protected constructor(protected router: Router,
                        protected navbarService: NavbarService,
                        protected route: ActivatedRoute) {
    this.dayStatuses = getEnumKeys(DayStatus, [DayStatus.CLOSED, DayStatus.E_WORK, DayStatus.NOSALE]);
  }

  public ngOnInit(): void {
    this.dateSubscription = dateRouteValueObservable(this.route, 'date').subscribe((date: Date) => {
      this._date = date;
      this.navbarService.setDate(date);
      this.preLoad();
    });

    this.pointIdSubscription = routeValueObservable(this.route, 'pointId').map(x => Number(x)).subscribe((pointId: number) => {
      this.pointId = pointId;

      if (this.user) {
        let points = this.user.points.filter((point: UserPoint) => point.point == this.pointId);

        if (points.length > 0) {
          this.navbarService.setSelectedPoint(points[0]);
        }
      }
    });

    this.navbarService.getUser()
      .subscribe((user: User) => {
        this.user = user;
        if (!this.point) {
          let points = user.points.filter((point: UserPoint) => point.point == this.pointId);

          if (points.length > 0) {
            this.navbarService.setSelectedPoint(points[0]);
          }
        }
      });

    this.navbarService.getSelectedPoint()
      .filter((point: UserPoint) => point.point == this.pointId)
      .subscribe((point: UserPoint) => {
        this.point = point;
        this.preLoad();
      });
  }

  protected preLoad() {
    if (this.user && this.point && this.point.point == this.pointId && this._date) {
      this.load();
    }
  }

  public ngOnDestroy(): void {
    if (this.actionsSubscription) {
      this.actionsSubscription.unsubscribe();
    }
    if (this.dateSubscription) {
      this.dateSubscription.unsubscribe();
    }
    if (this.pointIdSubscription) {
      this.pointIdSubscription.unsubscribe();
    }
  }

  protected abstract load(): void;

  public get date(): Date {
    return this._date;
  }

  public set date(date: Date) {
    if (!compareWithoutTime(date, this.date)) {
      this._date = date;
      //noinspection JSIgnoredPromiseFromCall
      this.router.navigate([new EditorUriPipe().transform(this.point), timestamp(dateWithoutTime(date))]);
    }
  }
}
