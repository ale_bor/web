import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/from";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/toArray";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {isJsObject} from "@angular/core/src/facade/lang";
import {environment} from "../../environments/environment";

export enum Method {
  GET,
  POST,
  PUT,
  PATCH,
  DELETE
}

export class ContentType {
  public static readonly X_WWW_FORM_URLENCODED: string = 'application/x-www-form-urlencoded; charset=UTF-8';
  public static readonly JSON: string = 'application/json; charset=UTF-8';
}

if (!Object.keys) {
  Object.keys = (function () {
    'use strict';
    let hasOwnProperty = Object.prototype.hasOwnProperty,
      hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
      dontEnums = [
        'toString',
        'toLocaleString',
        'valueOf',
        'hasOwnProperty',
        'isPrototypeOf',
        'propertyIsEnumerable',
        'constructor'
      ],
      dontEnumsLength = dontEnums.length;

    return function (obj) {
      if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
        throw new TypeError('Object.keys called on non-object');
      }

      let result = [], prop, i;

      for (prop in obj) {
        if (hasOwnProperty.call(obj, prop)) {
          result.push(prop);
        }
      }

      if (hasDontEnumBug) {
        for (i = 0; i < dontEnumsLength; i++) {
          if (hasOwnProperty.call(obj, dontEnums[i])) {
            result.push(dontEnums[i]);
          }
        }
      }
      return result;
    };
  }());
}

/**
 * Converts an object to a parametrised string.
 * @param object
 * @returns {string}
 */
export function objectToParams(object): string {
  return Object.keys(object).map((key) => isJsObject(object[key]) ?
    subObjectToParams(encodeURIComponent(key), object[key]) :
    `${encodeURIComponent(key)}=${encodeURIComponent(object[key])}`
  ).join('&');
}

/**
 * Converts a sub-object to a parametrised string.
 * @param key
 * @param object
 * @returns {string}
 */
function subObjectToParams(key, object): string {
  return Object.keys(object).map((childKey) => isJsObject(object[childKey]) ?
    subObjectToParams(`${key}[${encodeURIComponent(childKey)}]`, object[childKey]) :
    `${key}[${encodeURIComponent(childKey)}]=${encodeURIComponent(object[childKey])}`
  ).join('&');
}

export abstract class HttpHelper<D> {
  private readonly mainUrl: string = environment.apiUrl;

  constructor(protected _http: Http,
              protected _path: string = "/",
              protected _data: any = null,
              protected _method: Method = Method.GET,
              protected _contentType: string = ContentType.X_WWW_FORM_URLENCODED) {
  }

  //noinspection JSUnusedGlobalSymbols
  path(path: string): HttpHelper<D> {
    this._path = path;
    return this;
  }

  //noinspection JSUnusedGlobalSymbols
  data(data: any): HttpHelper<D> {
    this._data = data;
    return this;
  }

  //noinspection JSUnusedGlobalSymbols
  method(method: Method): HttpHelper<D> {
    this._method = method;
    return this;
  }

  get(): HttpHelper<D> {
    return this.method(Method.GET);
  }

  post(): HttpHelper<D> {
    return this.method(Method.POST);
  }

  //noinspection JSUnusedGlobalSymbols
  put(): HttpHelper<D> {
    return this.method(Method.PUT);
  }

  //noinspection JSUnusedGlobalSymbols
  patch(): HttpHelper<D> {
    return this.method(Method.PATCH);
  }

  //noinspection JSUnusedGlobalSymbols,ReservedWordAsName
  delete(): HttpHelper<D> {
    return this.method(Method.DELETE);
  }

  //noinspection JSUnusedGlobalSymbols
  removeData(): HttpHelper<D> {
    this._data = null;
    return this;
  }

  //noinspection JSUnusedGlobalSymbols
  contentType(contentType: string): HttpHelper<D> {
    this._contentType = contentType;
    return this;
  }

  build(): Observable<D> {
    return this.buildResponse()
      .catch(HttpHelper.handleError);
  }

  protected buildResponse(): Observable<Response> {
    let headers = new Headers({'Content-Type': this._contentType});
    let options = new RequestOptions({headers: headers, withCredentials: true});

    let body = this.buildBody();

    let url = `${this.mainUrl}${this._path}`;

    if (!environment.production) {
      console.debug(url, body);
    }

    let response: Observable<Response>;

    switch (this._method) {
      default:
      case Method.GET:
        if (this._data) {
          let separator = url.indexOf('?') > -1 ? '&' : '?';
          url = `${url}${separator}${body}`;
        }
        response = this._http.get(url, options);
        break;
      case Method.POST:
        response = this._http.post(url, body, options);
        break;
      case Method.PUT:
        response = this._http.put(url, body, options);
        break;
      case Method.PATCH:
        response = this._http.patch(url, body, options);
        break;
      case Method.DELETE:
        if (this._data) {
          let separator = url.indexOf('?') > -1 ? '&' : '?';
          url = `${url}${separator}${body}`;
        }
        response = this._http.delete(url, options);
        break;
    }

    return response;
  }

  protected buildBody(): string {
    if (this._contentType === ContentType.X_WWW_FORM_URLENCODED) {
      if (this._data) {
        return objectToParams(this._data);
      } else {
        return "";
      }
    } else {
      return JSON.stringify(this._data);
    }
  }

  public static handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}
