import {Sale} from "../models/sale/Sale";
import {Observable} from "rxjs";
import {compareWithoutTime} from "./date.helper";

export function contains(s: Sale, sales: Sale[]): boolean {
  for (let i in sales) {
    if (sales.hasOwnProperty(i)) {
      let sale: Sale = sales[i];
      if (s.compare(sale)) {
        return true;
      }
    }
  }
  return false;
}

export function filterContains(sales: Sale[], src: Sale[], date: Date): Observable<Sale> {
  return filterContainsObservable(Observable.from(sales), src, date);
}

export function filterContainsObservable(salesObservable: Observable<Sale>, src: Sale[], date: Date): Observable<Sale> {
  return salesObservable
    .filter((s: Sale) => compareWithoutTime(s.date, date))
    .filter((s: Sale) => !contains(s, src));
}
