import {ActivatedRoute, Params} from "@angular/router";
import {Observable} from "rxjs";

//TODO: переписать везде на Observable
export function routeNumberValue(route: ActivatedRoute, name: string): number {
  return Number(routeValue(route, name));
}

export function routeValue(route: ActivatedRoute, name: string): string {
  let result: string = null;
  route.params.forEach((params: Params) => result = params[name]);
  return result;
}

export function routeValueObservable(route: ActivatedRoute, name: string): Observable<string> {
  return route.params.filter((params: Params) => params && params[name]).map((params: Params) => params[name]);
}

export function dateRouteValueObservable(route: ActivatedRoute, name: string): Observable<Date> {
  return routeValueObservable(route, name).map(res => new Date(Number(res)));
}

export function createCheckUrl(checkType: string): string {
  return `/sales/edit/${checkType.toLowerCase()}`;
}
