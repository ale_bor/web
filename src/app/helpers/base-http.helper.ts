import {Router} from "@angular/router";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {DefaultResult} from "../models/DefaultResult";
import {HttpHelper, Method, ContentType} from "./http.helper";
import {handleDefaultResult} from "./response.helper";

export class BaseHttpHelper<R> extends HttpHelper<DefaultResult<R>> {
  private static _token: string = localStorage.getItem('token');

  constructor(http: Http,
              private router: Router,
              path: string = "/",
              data: any = null,
              method: Method = Method.GET,
              contentType: string = ContentType.X_WWW_FORM_URLENCODED) {
    super(http, path, data, method, contentType);
  }

  protected buildBody(): string {
    if (BaseHttpHelper._token) {
      if (!this._data) {
        this.data({});
      }
      this._data.token = BaseHttpHelper._token;
    }
    return super.buildBody();
  }

  build(): Observable<DefaultResult<R>> {
    return (<Observable<DefaultResult<R|any>|any>> super.build())
      .map(r => handleDefaultResult<R>(r, this.router));
  }

  static set token(token: string) {
    BaseHttpHelper._token = token;
  }
}
