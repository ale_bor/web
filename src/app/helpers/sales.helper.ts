import {Sale, SaleModel, SaleType} from "../models/sale/Sale";
import {ZReport, ZReportModel} from "../models/sale/ZReport";
// import {Short, ShortModel} from "../models/sale/Short";
import {Full, FullModel} from "../models/sale/Full";

export function saleFromModel(model: SaleModel): Sale {
  switch (SaleType[model.type]) {
    case SaleType.ZREPORT:
      return ZReport.fromModel(<ZReportModel>model);
    // case SaleType.SHORT:
    //   return Short.fromModel(<ShortModel>model);
    case SaleType.FULL:
      return Full.fromModel(<FullModel>model);
    default:
      return Sale.fromModel(model);
  }
}
