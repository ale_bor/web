import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RedTimePickerComponent} from "./red-time-picker.component";

@NgModule({
  imports: [CommonModule],
  exports: [RedTimePickerComponent],
  declarations: [RedTimePickerComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RedTimePickerModule {
}
