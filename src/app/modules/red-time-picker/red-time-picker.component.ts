import {Component, forwardRef, AfterViewInit} from "@angular/core";
import {NG_VALUE_ACCESSOR, ControlValueAccessor} from "@angular/forms";
import {currentDate} from "../../helpers/date.helper";

@Component({
  selector: 'red-time-picker',
  templateUrl: './red-time-picker.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RedTimePickerComponent),
      multi: true
    }
  ]
})
export class RedTimePickerComponent implements AfterViewInit, ControlValueAccessor {
  private _value: Date = currentDate();
  //noinspection JSUnusedLocalSymbols
  propagateChange = (_: Date) => {
  };

  ngAfterViewInit(): void {
    Inputmask().mask(document.querySelectorAll("input.timepicker"));
  }

  writeValue(value: Date): void {
    if (value) {
      this._value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }

  set time(time: string) {
    let hhmm = time.split(':');
    this._value.setHours(Number(hhmm[0]), Number(hhmm[1]));
    this.propagateChange(this._value);
  }

  get time(): string {
    let hours = RedTimePickerComponent.format_two_digits(this._value.getHours());
    let minutes = RedTimePickerComponent.format_two_digits(this._value.getMinutes());
    return hours + ":" + minutes;
  }

  private static format_two_digits(n) {
    return n < 10 ? '0' + n : n;
  }
}
