import {Component, ElementRef, AfterViewInit, Input, forwardRef} from "@angular/core";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {currentDate} from "../../helpers/date.helper";

@Component({
  selector: 'red-date-picker',
  templateUrl: './red-date-picker.component.html',
  styleUrls: ['./red-date-picker.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RedDatePickerComponent),
      multi: true
    }
  ]
})
export class RedDatePickerComponent implements AfterViewInit, ControlValueAccessor {
  @Input()
  options: DatapickerOptions = {
    autoclose: true,
    endDate: currentDate(),
    language: 'ru',
    weekStart: 1
  };

  private _value: Date = currentDate();
  private datePicker: DatePickerInterface;
  //noinspection JSUnusedLocalSymbols
  propagateChange = (_: Date) => {
  };

  constructor(private el: ElementRef) {
  }

  //noinspection JSUnusedGlobalSymbols
  ngAfterViewInit(): void {
    this.datePicker = $(this.el.nativeElement).find('.datepicker').datepicker(this.options);
    if (this._value) {
      this.datePicker.datepicker('setDate', this._value);
    }
    this.datePicker.on('changeDate', (e: changeDate) => this.propagateChange(e.date));
  }

  writeValue(value: Date): void {
    if (value) {
      this._value = value;
      if (this.datePicker) {
        this.datePicker.datepicker('setDate', this._value);
      }
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }
}
