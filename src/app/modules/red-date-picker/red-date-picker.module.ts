import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RedDatePickerComponent} from "./red-date-picker.component";

@NgModule({
  imports: [CommonModule],
  exports: [RedDatePickerComponent],
  declarations: [RedDatePickerComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RedDatePickerModule {
}

//thx https://github.com/uxsolutions/bootstrap-datepicker
