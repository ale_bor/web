declare interface DatapickerOptions {
  autoclose?: boolean;
  datesDisabled?: string[];
  daysOfWeekDisabled?: string[];
  defaultViewDate?: {year?: string, month?: string, day?: string};
  enableOnReadonly?: boolean;
  endDate?: string|Date;
  format?: string;
  language?: string;
  showOnFocus?: boolean;
  startDate?: string|Date;
  title?: string;
  todayBtn?: boolean;
  todayHighlight?: boolean;
  updateViewDate?: boolean;
  weekStart?: number;
}

declare interface changeDate {
  date: Date;
  dates: Date[];
  format([ix], [format]): void;
}

declare interface DatePickerInterface {
  on(eventName: string, data: any): DatePickerInterface;
  datepicker(method: string, args?): DatePickerInterface;
}

declare interface JQuery {
  datepicker(options?: DatapickerOptions): DatePickerInterface;
}
